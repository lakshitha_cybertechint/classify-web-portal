<?php
require_once 'ConnectionSetting.php';
/**
 * Created by Chinthaka Dilan F.
 * User: Bravo
 * Date: 2/10/2017
 * Time: 2:28 PM
 */
class Advertisement
{
    function __construct()
    {
        $db= new ConnectionSetting();
        $this->con=$db->connect();
    }

    //method for get posted by user
    function getMyAddCount($userID){
        $cmd="SELECT * FROM post_ad where user_id_2='".$userID."' AND status =1";
        $q=mysqli_query($this->con,$cmd);
        $adCount=mysqli_num_rows($q);
        return $adCount;
    }
}
