<?php session_start();
include 'connection.php';
?>
<?php 
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   }
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Register | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<!-- <li><a href="index-one.php">Category</a></li> -->
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
									<!-- <li><a href="pricing.php">Pricing</a></li> -->
									<!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>		
					<ul class="sign-in noscreen-res">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><a href="#"><?php echo $fname ?>&nbsp;<span class="caret"></span></a></dropdown><font style="color: #ffffff; font-weight: normal;">&nbsp;&nbsp;|&nbsp;&nbsp;<a href="logout.php">Log Out</a></font>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-ads.php">My Ads</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">My Profile</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">&nbsp;</a></li>
						    </ul>
						</li>
					</ul>
				<?php }else{ ?>
					<ul class="sign-in noscreen-res">
						<li><a href="signin.php"> Sign In </a></li>
						<li>&nbsp;<a href=""> | </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
				<?php } ?>
					<a href="ad-post-details.php" class="btn btn-post">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<!--mobile screen nav-right start-->
	<div class="nav-second">
	<?php if($_SESSION){?>		
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
			<li class="border-right"><a href="my-profile.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
			<li><a href="logout.php"> <img class="user-icon" src="images/icon/icon-logout.png"/> </a></li>
		</ul>
	<?php }else{ ?>
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
 		    <li class="border-right"><a href="contact-us.php"> <img class="user-icon" src="images/icon/icon-call.png"/> </a></li>
			<li> <a href="signin.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
		</ul>
	<?php } ?>
	</div>
	<!--mobile screen nav-right end-->

	<!-- signup-page -->
	<section id="main" class="clearfix user-page">
		<div class="container">
			<div class="row text-center">
				<!-- user-login -->			
				<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
					<div class="user-account">
						<h2>Create a Free Account</h2>
						<form action="register.php" method="post" name="register_field" id="register_field" onsubmit="return chk()">
							<div class="form-group">
								<input type="text" id="fname" name="fname" class="form-control col-md-3" placeholder="Enter your First Name" required="required" maxlength="12">
							</div>
							<div class="form-group">
								<input type="text" id="lname" name="lname" class="form-control col-md-3" placeholder="Enter your Last Name" required="required">
							</div>
							<div class="form-group">
								<input type="email" id="email" name="email" class="form-control" placeholder="Email address" required="required">
							</div>
							<div class="form-group">
								<input type="password" id="pword" name="pword" class="form-control" placeholder="Password" required="required">
							</div>
							<div class="form-group">
								<input type="password" id="pword_confirmed" name="pword_confirmed" class="form-control" placeholder="Confirm Password" required="required">
							</div>
							<div class="form-group">
								<input type="text" id="web" name="web" class="form-control col-md-3" placeholder="Enter your web site">
							</div>
							<select class="form-control" id="provinceFind" name="provinceFind">
								<option value="#">Select your Location</option>
							</select>
							<div class="form-group">
								<input type="text" id="mob_no" name="mob_no" class="form-control" placeholder="Mobile Number">
							</div>
							<div class="form-group">
								<input type="text" id="tel_no" name="tel_no" class="form-control" placeholder="Telephone Number">
							</div>
							
							<div class="checkbox">
								<label class="pull-left nonchecked" for="signing"><input type="checkbox" name="signing" id="signing" required="required"> By signing up for an account you agree to our Terms and Conditions </label>
							</div><!-- checkbox -->	
							<a href="#">
								<button type="submit" class="btn-submit form-control col-md-12 col-sm-12 col-xs-12">
								Sign up your new Account
							</button>
							</a>

							<hr>

							<a href="signin.php">
								<button type="button" class="btn-register form-control col-md-12 col-sm-12 col-xs-12">
								Sign in to exicting Account
							</button>
							</a>	
						</form>
						<!-- checkbox -->
										
					</div>
					<!-- <a href="signin.php" class="btn-primary">Already Registered!.. Sign in</a> -->
				</div><!-- user-login -->			
			</div><!-- row -->	
		</div><!-- container -->
	</section><!-- signup-page -->
	
	<!-- footer -->
	<footer id="footer" class="clearfix">
		<!-- <section class="footer-top clearfix">
			<div class="container">
				<div class="row">
				
					<div class="col-sm-3">
						<div class="footer-widget">
							<h3>Quik Links</h3>
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">All Cities</a></li>
								<li><a href="#">Help & Support</a></li>
								<li><a href="#">Advertise With Us</a></li>
								<li><a href="#">Blog</a></li>
							</ul>
						</div>
					</div>

			
					<div class="col-sm-3">
						<div class="footer-widget">
							<h3>How to sell fast</h3>
							<ul>
								<li><a href="#">How to sell fast</a></li>
								<li><a href="#">Membership</a></li>
								<li><a href="#">Banner Advertising</a></li>
								<li><a href="#">Promote your ad</a></li>
								<li><a href="#">Trade Delivers</a></li>
								<li><a href="#">FAQ</a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="footer-widget social-widget">
							<h3>Follow us on</h3>
							<ul>
								<li><a href="#"><i class="fa fa-facebook-official"></i>Facebook</a></li>
								<li><a href="#"><i class="fa fa-twitter-square"></i>Twitter</a></li>
								<li><a href="#"><i class="fa fa-google-plus-square"></i>Google+</a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i>youtube</a></li>
							</ul>
						</div>
					</div>

					
					<div class="col-sm-3">
						<div class="footer-widget news-letter">
							<h3>Newsletter</h3>
							<p>Trade is Worldest leading classifieds platform that brings!</p>
							
							<form action="#">
								<input type="email" class="form-control" placeholder="Your email id">
								<button type="submit" class="btn btn-primary">Sign Up</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section> -->

		
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->

   	<!--/Preset Style Chooser--> 
	<!-- <div class="style-chooser">
		<div class="style-chooser-inner">
			<a href="#" class="toggler"><i class="fa fa-life-ring fa-spin"></i></a>
			<h4>Presets</h4>
			<ul class="preset-list clearfix">
				<li class="preset1 active" data-preset="1"><a href="#" data-color="preset1"></a></li>
				<li class="preset2" data-preset="2"><a href="#" data-color="preset2"></a></li>
				<li class="preset3" data-preset="3"><a href="#" data-color="preset3"></a></li>        
				<li class="preset4" data-preset="4"><a href="#" data-color="preset4"></a></li>
			</ul>
		</div>
	</div> -->
	<!--/End:Preset Style Chooser-->
	
    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script> 
    <script src="js/jquery.countdown.js"></script>   
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
	<script type="text/javascript">


  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

	function chk(){
		var email = $("#con_email").val();
		var con_email = $("#email").val();
		if(email == con_email){
			$("#co1").text('Email Address is Confirmed');
		}else {
			$("#col").text('Email Address is not Confirmed');
		}
		
	}

	function chk(){
		var pword = $("#con_pword").val();
		var con_pword = $("#pword").val();
		if(pword == con_pword){
			$("#co2").text('password is Confirmed');
		}else {
			$("#co2").text('password is not Confirmed');
		}
		
	}

	function chk(){
		$("#f_name").text('');
		$("#l_name").text('');
		$("#addrstreet").text('');
		$("#addrcity").text('');
		$("#addrstate").text('');
		$("#mobno").text('');
		$("#telno").text('');
		$("#e-mail").text('');
		$("#co1").text('');
		$("#p_word").text('');
		$("#c_pword").text('');
    var fname = document.forms["register_field"]["fname"].value;
    var lname = document.forms["register_field"]["lname"].value;
    var addr_street = document.forms["register_field"]["addr_street"].value;
    var addr_city = document.forms["register_field"]["addr_city"].value;
    var addr_state = document.forms["register_field"]["addr_state"].value;
    var mob_no = document.forms["register_field"]["mob_no"].value;
    var tel_no = document.forms["register_field"]["tel_no"].value;
    var email = document.forms["register_field"]["email"].value;
    var con_email = document.forms["register_field"]["con_email"].value;
    var pword = document.forms["register_field"]["pword"].value;
    var cpword = document.forms["register_field"]["cpword"].value;
    if (fname == null || fname == "") {
        $("#f_name").text('First name must be filled out !');
        return false;
    }
    else if (lname == null || lname == "") {
        $("#l_name").text('Last name must be filled out !');
        return false;
    }
    else if (addr_street == null || addr_street == "") {
        $("#addrstreet").text('Please Enter your street No !');
        return false;
    }
    else if (addr_city == null || addr_city == "") {
        $("#addrcity").text('Please Enter your City !');
        return false;
    }
    else if (addr_state == null || addr_state == "") {
        $("#addrstate").text('Please Enter your State !');
        return false;
    }
    else if (mob_no == null || mob_no == "") {
        $("#mobno").text('Mobile Number is Required !');
        return false;
    }
    else if (tel_no == null || tel_no == "") {
        $("#telno").text('Telephone Number is Required !');
        return false;
    }
    else if (email == null || email == "") {
        $("#e-mail").text('E-mail Address is Required !');

        return false;
    }
    else if (con_email == null || con_email == "") {
        $("#co1").text('E-mail Address is Required !');
        return false;
    }
    else if (pword == null || pword == "") {
        $("#p_word").text('Password is Required !');
        return false;
    }
    else if (cpword == null || cpword == "") {
        $("#c_pword").text('Password is Required !');
        return false;
    }


}

	function ResetFunc() {
    	parent.window.location.reload('register_field');
}

$(document).ready(function(){  
		      $.ajax({ url: "get_province.php",
		          type: 'post',
		          success: function(data) {
		               var x = JSON.parse(data);
		               // console.log(x['name']);
		               for (var i = 0; i < x.length; i++) {
		                $("#provinceFind").append("<option value="+x[i]['did']+">"+x[i]['dname']+"</option>") ;
		               };
		          }
		      });

		});
</script>
  </body>
</html>