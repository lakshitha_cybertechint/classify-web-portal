<!DOCTYPE html>
<html lang="en">

<?php include 'connection.php';

?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Controller | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">

</head>
<body>

	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php"><i class="fa fa-home" style="font-size:18px; color:red;">&nbsp;</i><strong>Home</strong></a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<li><a href="index-one.php">Category</a></li>
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
									<li><a href="pricing.php">Pricing<!-- <span class="badge">New</span> --></a></li>
									<!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">

					<!-- sign-in -->					
					<ul class="sign-in">
						<li><i class="fa fa-user"></i></li>
						<li><a href="signin.php"> Sign In </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul><!-- sign-in -->					

					<a href="ad-post-details.php" class="btn">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

<?php

$query = "SELECT post_ad.id AS id, post_ad.price AS price, (select web from online_user where id=post_ad.user_id) AS web, post_ad.tittle AS ad_tittle, (select first_name from online_user where id=post_ad.user_id ) AS fname, (select last_name from online_user where id=post_ad.user_id ) AS lname, (select name from ad_pakage where ad_package_id=ad_pakage.id) AS package, post_ad.address AS address, post_ad.created_at AS created_at, (select mobile from online_user where id=post_ad.user_id ) AS mob_no, (select tel from online_user where id=post_ad.user_id ) AS tel_no, (select email from online_user where id=post_ad.user_id ) AS email, post_ad.description AS description FROM `post_ad` WHERE status=0 ORDER BY created_at ASC";
$result = $connection->query($query);
?>

<div class="row form-group add-category col-md-8 col-md-offset-1" style="margin-top: 30px;">
	<label class="col-md-3 label-category">Select Advertisement</label>
	<div class="col-sm-9">
		<select name="select_ad" id="select_ad" class="form-control">
			<option value="0">Select a Advertisement</option>
		</select>
	</div>
</div>

<table class="table table-striped">
	<?php
		while($row= $result->fetch_assoc()) {
	?>
    <thead>
    	<tr>
        	<th>id</th>
        	<td><?php echo $row['id'];?></td>
        </tr>
        <tr>
        	<th>Advertiser Name</th>
        	<td><?php echo $row['fname'];?> <?php echo $row['lname'];?></td>
        </tr>
        <tr>
        	<th>Email Address</th>
        	<td><?php echo $row['email'];?></td>
        </tr>
        <tr>
        	<th>Web Site</th>
        	<td><?php echo $row['web'];?></td>
        </tr>
        <tr>
        	<th>Mobile Number</th>
        	<td><?php echo $row['mob_no'];?></td>
        </tr>
        <tr>
        	<th>Telephone Number</th>
        	<td><?php echo $row['tel_no'];?></td>
        </tr>
        <tr>
        	<th>title</th>
        	<td><?php echo $row['ad_tittle'];?></td>
        </tr>
        <tr>
        	<th>Price</th>
        	<td><?php echo $row['price'];?></td>
        </tr>
        <tr>
        	<th>Negotiable</th>
        	<td><?php echo $row['negotiable'];?></td>
        </tr>
        <tr>
        	<th>category id</th>
        	<td><?php echo $row['cat_id'];?></td>
        </tr>
        <tr>
        	<th>sub category id</th>
        	<td><?php echo $row['sub_cat'];?></td>
        </tr>
        <tr>
        	<th>area</th>
        	<td><?php echo $row['area'];?></td>
        </tr>
        <tr>
        	<th>package</th>
        	<td><?php echo $row['package'];?></td>
        </tr>
        <tr>
        	<th>Additianally</th>
        	<td><?php echo $row['Additianally'];?></td>
        </tr>
        <tr>
        	<th>description</th>
        	<td><?php echo $row['description'];?></td>
        </tr>
        <tr>
        	<th>address</th>
        	<td><?php echo $row['address'];?></td>
        </tr>
        <tr>
        	<th>time stamp</th>
        	<td><?php echo $row['created_at'];?></td>
    	</tr>
    	<tr>
        	<th colspan="2" rowspan="2"><font color="#ff0000;"><b>*** Details Over ***</b></font></th>
    	</tr>
    </thead>
    <tbody>
    <?php
		}
	?>
    	
		<tr>
		    
			
			
			
			
			
			
			
			
			
		</tr>
	    
    </tbody>
  </table> 

<script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.countdown.js"></script>    
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">
	$(document).ready(function(){
      	$.ajax({ url: "get_ad.php",
          	type: 'post',
         		success: function(data){
            	var x = JSON.parse(data);
            		for (var i = 0; i < x.length; i++) {
            			$("#select_ad").append("<option value="+x[i]['id']+">"+x[i]['id']+"  +  "+x[i]['created_at']+"</option>");
               };

          }
      });

});
</script>

</body>