<?php
require_once 'ConnectionSetting.php';
/**
 * Created by Chinthaka Dilan F.
 * User: Bravo
 * Date: 2/10/2017
 * Time: 9:50 AM
 */
class LastLog
{

    function  __construct(){
        $db= new ConnectionSetting();
        $this->con=$db->connect();
        $this->DateTime=date("Y-m-d H:i:s ");
        $this->TimeZone=date("[e (\G\M\T P \h)]");
        if(isset($_SESSION['user_id'])){
            $this->userID=$_SESSION['user_id'];
        }
    }

    //method for insert user login time
    function setLoginTime(){
        //echo $this->DateTime ."<br>".$this->TimeZone ."<br>".$this->userID;
        $cmd="INSERT INTO last_log (user_id,log_in_time,Time_Zone) VALUES ('".$this->userID."','".$this->DateTime."','".$this->TimeZone."')";
        mysqli_query($this->con,$cmd);
        $this->getLoginSessionId();
    }

    //method for get current session id
    function getLoginSessionId(){
        $cmd="SELECT id FROM last_log WHERE user_id='".$this->userID."' AND log_in_time='".$this->DateTime."'";
        $query=mysqli_query($this->con,$cmd);
        while ($row=mysqli_fetch_array($query)){
            $sesId=$row['id'];
        }
        if(!isset($_SESSION['session_id'])){
            $_SESSION['session_id']=$sesId;
        }

    }

    //method for set user logout time
    function setLogOutTime(){
        if(isset($_SESSION['session_id'])){
            $sessionId=$_SESSION['session_id'];
            $cmd="UPDATE last_log SET log_out_time ='".$this->DateTime."',status=1 WHERE id='".$sessionId."'";
            mysqli_query($this->con,$cmd);
        }

    }

    //method for get user last login details
    function getLoginDetails(){
        $cmd="SELECT * FROM last_log WHERE user_id='".$this->userID."' ORDER BY log_in_time DESC LIMIT 1,1";
        $query=mysqli_query($this->con,$cmd);
        //if user has previous login
        if($query->num_rows>0){
            while ($row=mysqli_fetch_array($query)){
                $LastLogTime=$row['log_in_time'];
                $LastLogTimeZone=$row['Time_Zone'];
            }
            $details=$LastLogTime." ".$LastLogTimeZone;
        }
        //if user first login
        else{
            $cmd="SELECT * FROM last_log WHERE user_id='".$this->userID."' ORDER BY log_in_time DESC LIMIT 1";
            $query=mysqli_query($this->con,$cmd);
            while ($row=mysqli_fetch_array($query)){
                $LastLogTime=$row['log_in_time'];
                $LastLogTimeZone=$row['Time_Zone'];
            }
            $details=$LastLogTime." ".$LastLogTimeZone;
        }

        return $details;
    }


}
