<?php session_start();
 include 'connection.php';
?>
<?php 
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   }
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<?php include 'connection.php';?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Pricing | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li class="active"><a href="index.php"><i class="fa fa-home" style="font-size:18px; color:red;">&nbsp;</i><strong>Home</strong></a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<li><a href="index-one.php">Category</a></li>
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
									<li class="active"><a href="pricing.php"><strong>Pricing</strong><!-- <span class="badge">New</span> --></a></li>
									<!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>		
					<ul class="sign-in">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><font color="black">Hi!</font>&nbsp;&nbsp;&nbsp;<a href="#"><?php echo $fname ?><span class="caret"></span></a></dropdown>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" href="my-ads.php">My Account</a></li>
						      <li><a class="page-scroll" href="my-profile.php">Settings</a></li>
						      <li><a class="page-scroll" href="logout.php">Logout</a></li>
						    </ul>
						</li>
					</ul>
						<?php }else{ ?>
					<ul class="sign-in">
						<li><i class="fa fa-user"></i></li>
						<li><a href="signin.php"> Sign In </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
						<?php } ?>
					<a href="ad-post-details.php" class="btn">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<!-- main -->
	<section id="main" class="clearfix contact-us">
		<div class="container">

			<div class="pricing-section">
				<div class="row">
				<?php
					$sql = "SELECT * FROM ad_pakage where status=1";
					$result = $connection->query($sql);

					if ($result->num_rows > 0) {
					    // output data of each row
					    while($row = $result->fetch_assoc()) {
					    	?>
					    	<div class="col-sm-4">
								<div class="pric active">
								<?php if($row['is_most_popular']==1){;?>
								<div class="popular">
									<h2>Most Popular</h2>
								</div>	
								<?php }?>
									<h2><?php echo $row['name'];?></h2>
									<h3 style="font-size: 50px;"><sup>Rs</sup><?php echo number_format($row['price'],2);?><span> + STF <i class="icofont icofont-question-circle" data-toggle="tooltip" data-placement="top" title="Sales Transaction Fee is 3%" style="font-size:20px; opacity:0.7;"></i></span></h3>
									<div class="pric-menu">
										<ul>
										<?php
											$sql2 = "SELECT * FROM ad_feature where status=1";
											$result2 = $connection->query($sql2);

											if ($result2->num_rows > 0) {
											    // output data of each row
											    while($row2 = $result2->fetch_assoc()) {
											    	$sql3 = "SELECT * FROM ad_pakage_feature where status=1 and pakage_id=".$row['id']." AND feature_id=".$row2['id']."";
													$result3 = $connection->query($sql3);
													if ($result3->num_rows > 0) {
														?>
														<li><i class="fa fa-check" aria-hidden="true"></i><?php echo $row2['name'] ?></li>
														<?php
													}else{?>
														<li><i class="fa fa-times" aria-hidden="true"></i><?php echo $row2['name'] ?></li>
														<?php

													}

											    	?>
											    	
											    	<?php 
											    }
											}
									 	?>
											
										</ul>
										<!-- <a href="#" class="btn btn-primary">Buy Now</a> -->
									</div>
								</div>
							</div>
					    	<?php
					        
					    }
					} else {
					    echo "0 results";
					}
				?>
					

				</div><!-- row -->
			</div><!-- pricing section -->
		</div><!-- container -->
		
		
	</section><!-- main -->
	
	<!-- footer -->
	<footer id="footer" class="clearfix">
		
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->
	
     <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>   
    <script src="js/jquery.countdown.js"></script>  
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>