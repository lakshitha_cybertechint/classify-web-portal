<!DOCTYPE html>
<html xmlns="www.Classify.lk">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Construction | Classify.lk</title>
<meta name="Classifieds Lanka (pvt) Ltd" content="construction page" />

<link rel="icon" type="images/png" sizes="96x96" href="images/favicon.png">
<!-- <link href="cybertech_style.css" rel="stylesheet" type="text/css" /> -->
<!-- <script language="javascript" type="text/javascript">
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
</script> -->
</head>
<body>

<style type="text/css">
body {
  background-color: #eee;
}

body, h1, p {
  font-family: "Helvetica Neue", "Segoe UI", Segoe, Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: normal;
  margin: 0;
  padding: 0;
  text-align: center;
}

.container {
  margin-left:  auto;
  margin-right:  auto;
  margin-top: 177px;
  max-width: 1170px;
  padding-right: 15px;
  padding-left: 15px;
}

.row:before, .row:after {
  display: table;
  content: " ";
}

.col-md-6 {
  width: 50%;
}

.col-md-push-2 {
  margin-left: 25%;
}

h1 {
  font-size: 48px;
  font-weight: 300;
  margin: 0 0 20px 0;
}

h2 {
  font-size: 28px;
  font-weight: 300;
  margin: 0 0 20px 0;
}

.lead {
  font-size: 21px;
  font-weight: 400;
  margin-bottom: 20px;
}

p {
  margin: 0 0 10px;
}

a {
  color: #3282e6;
  text-decoration: none;
}
</style>

<div class="container text-center" id="error">
  <svg height="100" width="100">
    <polygon points="50,25 17,80 82,80" stroke-linejoin="round" style="fill:none;stroke:#ff8a00;stroke-width:8" />
    <text x="42" y="74" fill="#ff8a00" font-family="sans-serif" font-weight="900" font-size="42px">!</text>
  </svg>
 <div class="row">
    <div class="col-md-12">
      <div class="main-icon text-warning"><span class="uxicon uxicon-development"></span></div>
        <h1>We're under Development!</h1>
        <h2>(we will be back soon, be patience!)</h2>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6 col-md-push-2">
      <p class="lead">Need more Information?</p>
      <p class="lead">Email : <a href="mailto:contact@classify.lk">contact@classify.lk</a> call : <a href="tel:0713399099">(+94)71 33 99 099</a>.</p>
    </div>
  </div>
  <div>
  <?php echo "The system will redirect you to home in <span id='counter'>5</span> second(s).";
      header( "refresh:5;url=index.php" ); ?>
  </div>
</div>

<!-- <meta http-equiv="refresh" content="5;url=index.php"/> -->


<script type="text/javascript">
function countdown() {
    var i = document.getElementById('counter');
    if (parseInt(i.innerHTML)<=0) {
        location.href = 'index.php';
    }
    i.innerHTML = parseInt(i.innerHTML)-1;
}
setInterval(function(){ countdown(); },1000);
</script>

</body>
</html>
