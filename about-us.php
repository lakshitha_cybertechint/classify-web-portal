<?php session_start();
 include 'connection.php';
?>
<?php 
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   }
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>About Team | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<!-- <li><a href="index-one.php">Category</a></li> -->
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
									<!-- <li><a href="pricing.php">Pricing</a></li> -->
									<!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li class="active"><a href="about-us.php"><strong>About Us</strong></a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>		
					<ul class="sign-in noscreen-res">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><a href="#"><?php echo $fname ?>&nbsp;<span class="caret"></span></a></dropdown><font style="color: #ffffff; font-weight: normal;">&nbsp;&nbsp;|&nbsp;&nbsp;<a href="logout.php">Log Out</a></font>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-ads.php">My Ads</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">My Profile</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">&nbsp;</a></li>
						    </ul>
						</li>
					</ul>
				<?php }else{ ?>
					<ul class="sign-in noscreen-res">
						<li><a href="signin.php"> Sign In </a></li>
						<li>&nbsp;<a href=""> | </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
				<?php } ?>
					<a href="ad-post-details.php" class="btn btn-post">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<!--mobile screen nav-right start-->
	<div class="nav-second">
	<?php if($_SESSION){?>		
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
			<li class="border-right"><a href="my-profile.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
			<li><a href="logout.php"> <img class="user-icon" src="images/icon/icon-logout.png"/> </a></li>
		</ul>
	<?php }else{ ?>
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
 		    <li class="border-right"><a href="contact-us.php"> <img class="user-icon" src="images/icon/icon-call.png"/> </a></li>
			<li> <a href="signin.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
		</ul>
	<?php } ?>
	</div>
	<!--mobile screen nav-right end-->
	
	<!-- main -->
	<section id="main" class="clearfix about-us page">
		<div class="container">
		
			<div class="section about">
				<div class="about-info">
					<div class="row">
						<div class="col-md-6">
							<div class="about-us-images">
								<img src="images/about-us/1.jpg" alt="About us Image" class="img-responsive">
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="about-text">
								<h3>Who we are, And what we do</h3>
								<div class="description-paragraph">
									<p>classify.lk is a website where you can buy and sell almost everything. The best deals are often done with people who live in your own city or on your own street, so on classify.lk it's easy to buy and sell locally. All you have to do is select your region.</p>

									<p>It's completely free to publish a classified ad on classify.lk, and it takes you less than 2 minutes. You can post ads easily every time. just go to Post Your Ad, fill in the form, and you're done.</p>

									<p>classify.lk has the widest selection of popular second hand items all over Sri Lanka, which makes it easy to find exactly what you are looking for. So if you're looking for a car, mobile phone, house, computer or maybe a pet, you will find the best deal on classify.lk.</p>

									<p>classify.lk does not specialize in any specific category - here you can buy and sell items in more than 14 different categories. We also carefully review all ads that are being published, to make sure the quality is up to our standards.</p>

									<p>If you'd like to post an ad with us, just <a href="ad-post-details.php">click here</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				

				<div class="approach">
					<div class="row">
						<!-- <div class="col-sm-4 text-center">
							<div class="our-approach">
								<h3>Backgrounds</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
						</div>
						<div class="col-sm-4 text-center">
							<div class="our-approach">
								<h3>Our Approach</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat</p>
							</div>
						</div>
						<div class="col-sm-4 text-center">
							<div class="our-approach">
								<h3>Methodology</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
						</div> -->
					</div>
				</div>

				<div class="team-section">
					<h3>Classify Team Members</h3>
					<div class="team-members">
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/2.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Leaf Corcoran</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/3.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Mike Lewis</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/4.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Julie MacKay</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/5.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Christine Marquardt</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/6.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Loren Heiman</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/7.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Chris Taylor</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/8.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Alex Posey</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/9.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Teddy Newell</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/10.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul><!-- social -->
								</div>
							</div><!-- team-member-image -->
							<h4>Eli Amesefe</h4>
						</div><!-- team-member -->

						<!-- team-member -->
						<div class="team-member">
							<!-- team-member-image -->
							<div class="team-member-image">
								<img src="images/about-us/11.jpg" alt="Team Member" class="img-responsive">
								<!-- social -->
								<div class="team-social">
									<ul class="social">
										<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
										<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									</ul>
								</div>
							</div>
							<h4>Andrei Patru</h4>
						</div>
						
					</div>
				</div>
			</div>
			
			<!-- <div class="section testimonials text-center">
				<div class="testimonial-carousel">
					<div class="testimonial">
						<img src="images/about-us/12.jpg" alt="about Image" class="img-responsive">
						<h3 class="client-name">Lakshitha M Hettiarachchi</h3>
						<h4 class="client-company">Founder, Lakjaya Productions (pvt) Ltd</h4>
						<div class="client-pragrap">
							<p>“Wow!! It's really awesome. Nice and Clean design.It's really impressive. I am<br> really appreciate your project.”</p>
						</div>
					</div>

					<div class="testimonial">
						<img src="images/about-us/4.jpg" alt="Image" class="img-responsive">
						<h3 class="client-name">Hena Rio</h3>
						<h4 class="client-company">CEO, Leo Inc</h4>
						<div class="client-pragrap">
							<p>“Wow!! It's really awesome. Nice and Clean design.It's really impressive. I am<br> really appreciate your project.”</p>
						</div>
					</div>

					<div class="testimonial">
						<img src="images/about-us/6.jpg" alt="" class="img-responsive">
						<h3 class="client-name">Jhon Mark</h3>
						<h4 class="client-company">Founder, Mark Ltd.</h4>
						<div class="client-pragrap">
							<p>“Wow!! It's really awesome. Nice and Clean design.It's really impressive. I am <br> really appreciate your project.”</p>
						</div>
					</div>				
				</div>
			</div> -->

		</div><!-- container -->
	</section><!-- main -->
	
	<!-- footer -->
	<footer id="footer" class="clearfix">
		
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->
	
     <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script> 
    <script src="js/jquery.countdown.js"></script>   
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
	<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>