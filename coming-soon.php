<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>We are on the way | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
  	<div class="upcoming-section">
  		<div class="upcoming-overlay"></div>
  		<div class="container">
  			<div class="logo-intro">
  				<a href="index.php"><img class="img-responsive" src="images/logo2.png" width="20%" alt="Logo"></a>
  			</div>
  			<h1></h1>
  			<h2><span>We are under costruction!, Launching in</h2>
			<ul id="countdown">
			    <li>                    
			        <span class="days">00</span>
			        <p>days</p>
			    </li>
			    <li>
			        <span class="hours">00</span>
			        <p>hours</p>
			    </li>
			    <li>
			        <span class="minutes">00</span>
			        <p>minutes</p>
			    </li>
			    <li>
			        <span class="seconds">00</span>
			        <p>seconds</p>
			    </li>               
			</ul>
			<ul>
				<li><a href="#about" class="page-scroll"><button type="button" class="btn btn-primary">More Info</button></a></li>
			</ul> 
 			<ul class="socail list-inline">
				<li><a href="#" title="Facebook"><i class="fa fa-facebook"></i></a></li>
				<li><a href="#" title="Twitter"><i class="fa fa-twitter"></i></a></li>
				<li><a href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
				<li><a href="#" title="Youtube"><i class="fa fa-youtube"></i></a></li>
			</ul>
  		</div><!-- container -->
  	</div><!-- upcoming section -->

  	
  	<div class="about-trade">
  		<div class="container">
  			<div class="about-title">
  				<section id="about">
	  			<div class="logo-intro text-center">
	  				<img class="img-responsive" src="images/logo2.png" width="40%" alt="Logo">
	  			</div>  		
	  			<h2 class="text-center">About <span>Classify.lk</span></h2>  				
  			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="about">
						<p>The founder and CEO of the agency is a professional web designer and has been desiging website for decades. Cairo is an upcoming group of some creative people. </p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="about">
						<p>The founder and CEO of the agency is a professional web designer and has been desiging website for decades. Cairo is an upcoming group of some creative people. </p>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="about">
						<p>The founder and CEO of the agency is a professional web designer and has been desiging website for decades. Cairo is an upcoming group of some creative people. </p>
					</div>
				</div>
			</div><!-- row -->
			<div class="about-info">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>
				<a href="#">Robert Geek, CEO</a>
			</div>
  		</div><!-- container -->
  	</div><!-- about-trade -->
  	</section>

  	<div class="col-to-action parallax-section">
  		<div class="container">
  			<div class="contact">
  				<h1>Subscribe Our Newsletter</h1>
  				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
  				<div class="row">
  					<div class="col-sm-8 col-sm-offset-2">
		  				<form action="#" class="contact-form" name="contact-form" method="post" >
							<div class="form-group">
								<input type="email" class="form-control" required="required" placeholder="Enter Your Email Id">						
							</div> 
							<div class="form-group">
								<button type="submit" class="btn"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
							</div>					
		  				</form>  						
  					</div>
  				</div>
  			</div>
  		</div><!-- container -->
  	</div><!-- col-to-action -->

  	<div class="contac-section">
  		<div class="container">
  			<div class="touch-content">
  				<h1>Get in Touch</h1>
  				<div class="row">
  					<div class="col-sm-8 col-sm-offset-2">
						<form id="contact-form" class="contact-form" name="contact-form" method="post" action="#">
							<div class="form-group">
								<input type="text" class="form-control" required="required" placeholder="Full Name">
							</div>
							<div class="form-group">
								<input type="email" class="form-control" required="required" placeholder="Email Id">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" required="required" placeholder="Subject">
							</div> 
							<div class="form-group">
							    <textarea name="message" required="required" class="form-control" rows="7" placeholder="Your Tex"></textarea>
							</div>               
							<div class="form-group pull-right">
								<button type="submit" class="btn btn-primary">Submit Your Tex</button>
							</div>
						</form>   						
  					</div>
  				</div> 				
  			</div>
  		</div><!-- container -->
  	</div><!-- contac-section -->


	<footer id="footer" class="clearfix">
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->
	
    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>    
    <script src="js/jquery.countdown.js"></script>    
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>

	<script>
	    (function () {

	        $("#countdown").countdown({
	            date: "18 September 2016 17:04:00",
	            format: "on"
	        });
	    
	    }());   		
	</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

  </body>
</html>