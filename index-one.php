<?php session_start();
 include 'connection.php';
?>
<?php 
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   }
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<?php


$query = "SELECT 
	ct.*,
	IFNULL(tmp1.add_count,0) as count
from category ct
left join (
	select 
		category_id,
		count(id) as add_count
	from post_ad WHERE status=1 group by category_id
)tmp1 on tmp1.category_id = ct.id where ct.level=1 order by ct.id ASC";
$result = $connection->query($query);

?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Category | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php"><i class="fa fa-home" style="font-size:18px; color:red;">&nbsp;</i>Home</a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<li class="active"><a href="index-one.php"><strong>Category</strong></a></li>
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="about-us.php">ABout Us</a></li>
									<li><a href="contact-us.php">Contact Us</a></li>
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="ad-post-details.php">Ad post Details</a></li>
									<li><a href="categories-main.php">Category Ads</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="signup.php">Sign Up</a></li>
									<li><a href="signin.php">Sign In</a></li>
									<li><a href="faq.php">FAQ</a></li>	
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li>
									<li><a href="pricing.php">Pricing<span class="badge">New</span></a></li>
									<li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>		
					<ul class="sign-in">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><font color="black">Hi!</font>&nbsp;&nbsp;&nbsp;<a href="#"><?php echo $fname ?><span class="caret"></span></a></dropdown>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" href="my-ads.php">My Account</a></li>
						      <li><a class="page-scroll" href="my-profile.php">Settings</a></li>
						      <li><a class="page-scroll" href="logout.php">Logout</a></li>
						    </ul>
						</li>
					</ul>
						<?php }else{ ?>
					<ul class="sign-in">
						<li><i class="fa fa-user"></i></li>
						<li><a href="signin.php"> Sign In </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
						<?php } ?>
					<a href="ad-post-details.php" class="btn">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<!-- main -->
	<section id="main" class="clearfix home-default">
		<div class="container">
			<!-- banner -->
			
			
			<!-- main-content -->
			<div class="main-content">
				<!-- row -->
				<div class="row">
					<div class="hidden-xs hidden-sm col-md-2 text-center">
						<div class="advertisement" style="margin-bottom:20px;">
							<a href="#"><img src="images/ads/index-vertical-1.gif" alt="Images" class="img-responsive"></a>
						</div>
						<div class="advertisement" style="margin-bottom:20px;">
							<a href="#"><img src="images/ads/index-vertical-1.gif" alt="Images" class="img-responsive"></a>
						</div>
					</div>
					
					<!-- product-list -->
					
					<div class="col-md-8">
						<!-- categorys -->
						<section id="category-thumbnail" style="margin-top:-50px;">
							<div id="category-thumbnail" class="section category-ad text-center">
								<ul class="category-list">	
									<?php 

								while($row = $result->fetch_assoc()) {?>
								<li class="category-item">
										<a href="categories.php">
											<div class="category-icon" style="font-size:48px; opacity:0.7;"><a href="categories-main.php?category=<?php echo $row['id']; ?>&province=0&pg=0"><i class="<?php echo $row['icon']; ?>"></i></div>
											<span class="category-title"><?php echo $row['name']; ?></span>
											<span class="category-quantity">(<?php echo $row['count']; ?>)</span></a>
										</a>
									</li>
								 	
								 	
								<?php }?>
								</ul>				
							</div>
						<!-- category-ad -->	
						</section>

						<div class="ad-section text-center" style="margin-top:-40px;">
							<a href="http://www.cybertech.lk/" target="_blank"><img src="images/ads/index-horizontal-1.gif" alt="Image" class="img-responsive"></a>
						</div>

						<!-- featureds -->
						<!-- <div class="section featureds">
							<div class="row">
								<div class="col-sm-12">
									<div class="section-title featured-top">
										<h4>Top Advertisments</h4>
									</div>
								</div>
							</div>
							<?php 
							$top_ads_query = "SELECT post_ad.id AS id, post_ad.price AS ad_price,post_ad.tittle AS ad_tittle,category.name As cat_name,(select name from category where id=post_ad.sub_category_id ) AS sub_cat,post_ad.created_at AS cre_date,post_ad.item_condition As ic, (select dname from district where did=post_ad.area_id) AS location FROM `post_ad` INNER JOIN category ON post_ad.category_id=category.id INNER JOIN ad_pakage ON post_ad.ad_package_id=ad_pakage.id where post_ad.status= 1 AND ad_pakage.is_top_ad=1 ORDER BY cre_date DESC LIMIT 10";
							$result_top_ads_ = $connection->query($top_ads_query);
							?>

							<div class="featured-slider">
								<div id="featured-slider" >
									<?php 
									while($row_topADS = $result_top_ads_->fetch_assoc()) {
										$query1_row_topADS = "SELECT * FROM ads_image WHERE ad_id=".$row_topADS['id']." LIMIT 1";
											$result1_row_topADS = $connection->query($query1_row_topADS);	?>
										<div class="featured">
										<div class="featured-image">
											<?php while($row1_row_topADS = $result1_row_topADS->fetch_assoc()) { 
												$bodytag = str_replace("../", "", $row1_row_topADS['url'] );
											?>
												<a href="details.php?id=<?php echo $row_topADS['id']; ?>"><img src="<?php echo $bodytag ; ?>" alt="Image" class="img-responsive"></a>
											<?php }?>
											
											<a href="details.php?id=<?php echo $row_topADS['id']; ?>" class="verified" data-toggle="tooltip" data-placement="left" title="Top Ad"><i class="fa fa-check-square-o"></i></a>
										</div>
									
										<div class="ad-info text-center">
											<a href="details.php?id=<?php echo $row_topADS['id']; ?>"><h3 style="font-size:15px;" class="item-price"><?php echo $row_topADS['ad_tittle']?></h3></a>
											<a href="details.php?id=<?php echo $row_topADS['id']; ?>"><h4 class="item-title">Rs <?php echo number_format($row_topADS['ad_price'],2)?></h4></a>
											<div class="item-cat">
												<span style="margin-top: 20px;"><a href="details.php?id=<?php echo $row_topADS['id']; ?>"><?php echo $row_topADS['sub_cat']?></a></span>
											</div>
										</div>
										
										<div class="ad-meta">
											<div class="meta-content">
												<span class="dated"><a href="details.php?id=<?php echo $row_topADS['id']; ?>"><?php echo $row_topADS['location']?> region</a></span>
											</div>									

											<div class="user-option pull-right">
												
												
											</div>
										</div>
									</div>
								
								<?php } ?>
									
								</div>
							</div>
						</div> --><!-- featureds	

						<!-- trending-ads -->
						<div class="section trending-ads">
							<?php 
							$top_ads_query = "SELECT post_ad.id AS id, post_ad.price AS ad_price,post_ad.tittle AS ad_tittle,category.name As cat_name,(select name from category where id=post_ad.sub_category_id ) AS sub_cat,post_ad.created_at AS cre_date,post_ad.item_condition As ic, (select dname from district where did=post_ad.area_id) AS location FROM `post_ad` INNER JOIN category ON post_ad.category_id=category.id INNER JOIN ad_pakage ON post_ad.ad_package_id=ad_pakage.id where post_ad.status= 1 AND ad_pakage.is_top_ad=1 ORDER BY cre_date DESC LIMIT 10";
							$result_top_ads_ = $connection->query($top_ads_query);
							?>
							<div class="section-title tab-manu">
								<h4>Top Ads</h4>
								 <!-- Nav tabs -->      
								<ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"><a href="#recent-ads"  data-toggle="tab">Recent Ads</a></li>
									<!-- <li role="presentation"><a href="#popular" data-toggle="tab">Popular Ads</a></li>
									<li role="presentation"><a href="#hot-ads"  data-toggle="tab">Hot Ads</a></li> -->
								</ul>
							</div>

				  			<!-- Tab panes -->
							<div class="tab-content">
								<!-- tab-pane -->
								<div role="tabpanel" class="tab-pane fade in active" id="recent-ads">
									<?php 
										while($row_topADS = $result_top_ads_->fetch_assoc()) {
											$query1_row_topADS = "SELECT * FROM ads_image WHERE ad_id=".$row_topADS['id']." LIMIT 1";
												$result1_row_topADS = $connection->query($query1_row_topADS);
									?>
									<!-- ad-item -->
									<div class="ad-item row">
										<!-- item-image -->
										<?php while($row1_row_topADS = $result1_row_topADS->fetch_assoc()) { 
											$bodytag = str_replace("../", "", $row1_row_topADS['url'] );
										?>
										<div class="item-image-box col-sm-4">
											<div class="item-image">
												<a href="details.php?id=<?php echo $row_topADS['id']; ?>"><img src="<?php echo $bodytag ; ?>" alt="Image" class="img-responsive"></a>
												<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Top Ad"><i class="fa fa-check-square-o"></i></a>
											</div><!-- item-image -->
										</div>
										<?php }?>
										
										<!-- rending-text -->
										<div class="item-info col-sm-8">
											<!-- ad-info -->
											<div class="ad-info">
												<a href="details.php?id=<?php echo $row_topADS['id']; ?>"><h3 style="font-size:18px;" class="item-price"><?php echo $row_topADS['ad_tittle']?></h3></a>
											<a href="details.php?id=<?php echo $row_topADS['id']; ?>"><h4 class="item-title">Rs <?php echo number_format($row_topADS['ad_price'],2)?></h4></a>
												<div class="item-cat">
													<span><a href="details.php?id=<?php echo $row_topADS['id']; ?>"><?php echo $row_topADS['sub_cat']?></a></span>
												</div>	
											</div><!-- ad-info -->
											
											<!-- ad-meta -->
											<div class="ad-meta">
												<div class="meta-content">
													<span class="dated"><a href="#"><?php echo $row_topADS['cre_date']?></a></span>
													<!-- <a href="#" class="tag"><i class="fa fa-tags"></i> Used</a> -->
												</div>

												<!-- item-info-right -->
												<div class="user-option pull-right">
													<a href="details.php?id=<?php echo $row_topADS['id']; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $row_topADS['location']?>"><i class="fa fa-map-marker"></i> </a>
													<!-- <a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Dealer"><i class="fa fa-suitcase"></i> </a> -->											
												</div><!-- item-info-right -->
											</div><!-- ad-meta -->
										</div><!-- item-info -->
									</div><!-- ad-item -->
									<?php } ?>
								</div><!-- tab-pane -->
								
								<!-- tab-pane -->
								<div role="tabpanel" class="tab-pane fade" id="popular">

									<div class="ad-item row">
										<!-- item-image -->
										<div class="item-image-box col-sm-4">
											<div class="item-image">
												<a href="details.html"><img src="images/trending/1.jpg" alt="Image" class="img-responsive"></a>
												<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Verified"><i class="fa fa-check-square-o"></i></a>
											</div><!-- item-image -->
										</div>
										
										<!-- rending-text -->
										<div class="item-info col-sm-8">
											<!-- ad-info -->
											<div class="ad-info">
												<h3 class="item-price">$50.00</h3>
												<h4 class="item-title"><a href="#">Apple TV - Everything you need to know!</a></h4>
												<div class="item-cat">
													<span><a href="#">Electronics & Gedgets</a></span> /
													<span><a href="#">Tv & Video</a></span>
												</div>	
											</div><!-- ad-info -->
											
											<!-- ad-meta -->
											<div class="ad-meta">
												<div class="meta-content">
													<span class="dated"><a href="#">7 Jan, 16  10:10 pm </a></span>
													<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>
												</div>									
												<!-- item-info-right -->
												<div class="user-option pull-right">
													<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
													<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Dealer"><i class="fa fa-suitcase"></i> </a>											
												</div><!-- item-info-right -->
											</div><!-- ad-meta -->
										</div><!-- item-info -->
									</div><!-- ad-item -->									
								</div><!-- tab-pane -->

								<!-- tab-pane -->
								<div role="tabpanel" class="tab-pane fade" id="hot-ads">
									<!-- ad-item -->
									<div class="ad-item row">
										<!-- item-image -->
										<div class="item-image-box col-sm-4">
											<div class="item-image">
												<a href="details.html"><img src="images/trending/1.jpg" alt="Image" class="img-responsive"></a>
												<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Verified"><i class="fa fa-check-square-o"></i></a>
											</div><!-- item-image -->
										</div>
										
										<!-- rending-text -->
										<div class="item-info col-sm-8">
											<!-- ad-info -->
											<div class="ad-info">
												<h3 class="item-price">$50.00</h3>
												<h4 class="item-title"><a href="#">Apple TV - Everything you need to know!</a></h4>
												<div class="item-cat">
													<span><a href="#">Electronics & Gedgets</a></span> /
													<span><a href="#">Tv & Video</a></span>
												</div>	
											</div><!-- ad-info -->
											
											<!-- ad-meta -->
											<div class="ad-meta">
												<div class="meta-content">
													<span class="dated"><a href="#">7 Jan, 16  10:10 pm </a></span>
													<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>
												</div>									
												<!-- item-info-right -->
												<div class="user-option pull-right">
													<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>
													<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Dealer"><i class="fa fa-suitcase"></i> </a>											
												</div><!-- item-info-right -->
											</div><!-- ad-meta -->
										</div><!-- item-info -->
									</div><!-- ad-item -->								
								</div><!-- tab-pane -->
							</div>
						</div><!-- trending-ads -->

						<!-- ad-section -->						
						<div class="ad-section text-center">
							<a href="http://www.cybertech.lk/" target="_blank"><img src="images/ads/index-one-horizontal-2.gif" alt="Image" class="img-responsive"></a>
						</div><!-- ad-section -->
						<!-- cta -->
						<div class="section cta text-center">
							<div class="row">
								<!-- single-cta -->
								<div class="col-sm-4">
									<div class="single-cta">
										<!-- cta-icon -->
										<div class="cta-icon icon-secure">
											<img src="images/icon/13.png" alt="Icon" class="img-responsive">
										</div><!-- cta-icon -->

										<h4>Secure Trading</h4>
										<!-- <p>Duis autem vel eum iriure dolor in hendrerit in</p> -->
									</div>
								</div><!-- single-cta -->

								<!-- single-cta -->
								<div class="col-sm-4">
									<div class="single-cta">
										<!-- cta-icon -->
										<div class="cta-icon icon-support">
											<img src="images/icon/14.png" alt="Icon" class="img-responsive">
										</div><!-- cta-icon -->

										<h4>24/7 Support</h4>
										<!-- <p>Duis autem vel eum iriure dolor in hendrerit in</p> -->
									</div>
								</div><!-- single-cta -->

								<!-- single-cta -->
								<div class="col-sm-4">
									<div class="single-cta">
										<!-- cta-icon -->
										<div class="cta-icon icon-trading">
											<img src="images/icon/15.png" alt="Icon" class="img-responsive">
										</div><!-- cta-icon -->

										<h4>Easy Trading</h4>
										<!-- <p>Duis autem vel eum iriure dolor in hendrerit in</p> -->
									</div>
								</div><!-- single-cta -->
							</div><!-- row -->
						</div><!-- cta -->
					</div><!-- product-list -->

					<!-- advertisement -->
					<div class="hidden-xs hidden-sm col-md-2">
						<div class="advertisement text-center" style="margin-bottom:20px;">
							<a href="#"><img src="images/ads/index-vertical-1.gif" alt="Images" class="img-responsive"></a>
						</div>
						<div class="advertisement text-center" style="margin-bottom:20px;">
							<a href="#"><img src="images/ads/index-vertical-1.gif" alt="Images" class="img-responsive"></a>
						</div>
					</div><!-- advertisement -->
				</div><!-- row -->
			</div><!-- main-content -->
		</div><!-- container -->
	</section><!-- main -->
	
	<!-- download -->
	<section id="download" class="clearfix parallax-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2>Download on App Store</h2>
				</div>
			</div><!-- row -->

			<!-- row -->
			<div class="row">
				<!-- download-app -->
				<div class="col-sm-4">
					<a href="#" class="download-app">
						<img src="images/icon/16.png" alt="Image" class="img-responsive">
						<span class="pull-left">
							<span>available on</span>
							<strong>Google Play</strong>
						</span>
					</a>
				</div><!-- download-app -->

				<!-- download-app -->
				<div class="col-sm-4">
					<a href="#" class="download-app">
						<img src="images/icon/17.png" alt="Image" class="img-responsive">
						<span class="pull-left">
							<span>available on</span>
							<strong>App Store</strong>
						</span>
					</a>
				</div><!-- download-app -->

				<!-- download-app -->
				<div class="col-sm-4">
					<a href="#" class="download-app">
						<img src="images/icon/18.png" alt="Image" class="img-responsive">
						<span class="pull-left">
							<span>available on</span>
							<strong>Windows Store</strong>
						</span>
					</a>
				</div><!-- download-app -->
			</div><!-- row -->
		</div><!-- contaioner -->
	</section><!-- download -->

	<!-- footer -->
	<footer id="footer" class="clearfix">
		
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->
	
     <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>  
    <script src="js/jquery.countdown.js"></script>  
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>