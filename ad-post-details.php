<?php session_start();
 include 'connection.php';
?>
<?php 
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   	   $lname=$row['last_name'] ;
	   	   $mob_no=$row['mobile_number'] ;
	   	   $tel_no=$row['tel_no'] ;
	   	   $web=$row['web'] ;
	   	   $email=$row['email'] ;
	   }
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<?php ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Post your Ad | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
    <script src="js/CharCount.js"></script>
    <script type="text/javascript">
      function countChar(val) {
        var len = val.value.length;
        if (len >= 5000) {
          val.value = val.value.substring(0, 5000);
        } else {
          $('#charNum').text(5000 - len);
        }
      };
    </script>
</head>
<body>

<style type="text/css">

  	#popup {
            display:none;
            position:absolute;
            margin:0 auto;
            /*top: 20%;
            left: 20%;
            transform: translate(-12%, -20%);*/
            z-index: 9999;
    }

	#newsletter_background {
	    position: fixed;
	    top: 0px;
	    right: 0px;
	    bottom: 0px;
	    left: 0px;
	    opacity: 0.8;
	    z-index: 9998;
	    cursor: pointer;
	    background-color: black;
	}

</style>
<!-- <div>
	<div id="newsletter_background" hidden="hidden"></div>
	<div id="popup" class="col-md-12">
	<section id="main" class="clearfix ad-post-page col-md-12">
		<div class="container col-md-12">

			<?php if($_SESSION){?>		
					<div class="breadcrumb-section">
						<h3 class="header text-center">Welcome <?php echo $fname ?>! Let's post your ad. Select Advertisement category below</h3>
					</div>
				<?php }else{ ?>
					<div class="breadcrumb-section">
						<h3 class="header text-center">Welcome Guest! Let's post your ad. Select Advertisement category below</h3>
					</div>
				<?php } ?>
				
			<div id="ad-post">
				<div class="row category-tab">
					<div class="col-md-4 col-sm-6">
						<div class="section cat-option select-category post-option">
							<h4>Select a category</h4>
							<ul role="tablist">
								<li>
									<a href="#cat1" aria-controls="cat1" role="tab" data-toggle="tab">
										<span class="select">
											<img src="images/icon/1.png" alt="Images" class="img-">
										</span>
										Cars & Vehicles
									</a>
								</li>
								<li>
									<a href="#cat2" aria-controls="cat2" role="tab" data-toggle="tab">
										<span class="select">
											<img src="images/icon/1.png" alt="Images" class="img-">
										</span>
										Electronic
									</a>
								</li>
							</ul>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-6">
						<div class="section tab-content subcategory post-option">
							<h4>Select a subcategory</h4>
							<div role="tabpanel" class="tab-pane" id="cat1">
								<ul>
									<li><a href="javascript:void(0)">Cars & Buses</a></li>
									<li><a href="javascript:void(0)">Motorbikes & Scooters</a></li>
									<li><a href="javascript:void(0)">Bicycles and Three Wheelers</a></li>
									<li><a href="javascript:void(0)">Three Wheelers</a></li>
									<li><a href="javascript:void(0)">Trucks, Vans & Buses</a></li>
									<li><a href="javascript:void(0)">Tractors & Heavy-Duty</a></li>
									<li><a href="javascript:void(0)">Auto Parts & Accessories</a></li>
								</ul>	
							</div>
							<div role="tabpanel" class="tab-pane" id="cat2">
								<ul>
									<li><a href="javascript:void(0)">Laptop & Computer</a></li>
									<li><a href="javascript:void(0)">Mobile Phones</a></li>
									<li><a href="javascript:void(0)">Phablet & Tablets</a></li>
									<li><a href="javascript:void(0)">Audio & MP</a></li>
									<li><a href="javascript:void(0)">Accessories</a></li>
									<li><a href="javascript:void(0)">Cameras</a></li>
									<li><a href="javascript:void(0)">Mobile Accessories</a></li>
									<li><a href="javascript:void(0)">TV & Video</a></li>
									<li><a href="javascript:void(0)">Other Electronics</a></li>
									<li><a href="javascript:void(0)">TV & Video Accessories</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat3">
								<ul>
									<li><a href="javascript:void(0)">Houses & Plots</a></li>
									<li><a href="javascript:void(0)">Lands & property</a></li>
									<li><a href="javascript:void(0)">Plots & Lands</a></li>
									<li><a href="javascript:void(0)">Apartment</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat4">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat5">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat6">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat7">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat8">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat9">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat10">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat11">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="section post-option">
							<h4>Please click to submit!</h4>
								<p></p>
								<a id="next1" data-toggle="tab" href="#">
									<button class="btn btn-next margin-top-60" onclick="check_ad_details();">PROCEED TO NEXT</button>
								</a>
							
						</div>
					</div>
				</div>
			</div>				
		</div>
	</section>
	</div>
</div> -->

<script src="js/popUp.js"></script>

<script type="text/javascript">
  	$(document).ready(function () {

    $("#popup").hide().fadeIn(1000);
    $("#newsletter_background").fadeIn(1000);

    $("#close").on("click", function (e) {
        e.preventDefault();
        $("#popup").fadeOut(1000);
        $("#newsletter_background").fadeOut(1000);
    });

});

    $(document).ready(function () {

    $("#popup").hide().fadeIn(1000);
    $("#newsletter_background").fadeIn(1000);

    $("#newsletter_background").on("click", function (e) {
        e.preventDefault();
        $("#popup").fadeOut(1000);
        $("#newsletter_background").fadeOut(1000);
    });

});

    $(document).ready(function () {

    $("#popUpImg").on("click", function (e) {
        e.preventDefault();
        $("#popUpImg").fadeOut(1000);
        $("#popup").hide().fadeOut(1000);
        $("#close").hide().fadeOut(1000);
        $("#newsletter_background").fadeOut(1000);
    });

});

    $(document).ready(function () {

    $("#Image_div").on("click", function (e) {
        e.preventDefault();
        $("#popUpImg").fadeOut(1000);
        $("#popup").hide().fadeOut(1000);
        $("#close").hide().fadeOut(1000);
        $("#newsletter_background").fadeOut(1000);
    });

});
</script>

	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<!-- <li><a href="index-one.php">Category</a></li> -->
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
									<!-- <li><a href="pricing.php">Pricing</a></li> -->
									<!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>		
					<ul class="sign-in noscreen-res">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><a href="#"><?php echo $fname ?>&nbsp;<span class="caret"></span></a></dropdown><font style="color: #ffffff; font-weight: normal;">&nbsp;&nbsp;|&nbsp;&nbsp;<a href="logout.php">Log Out</a></font>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-ads.php">My Ads</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">My Profile</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">&nbsp;</a></li>
						    </ul>
						</li>
					</ul>
				<?php }else{ ?>
					<ul class="sign-in noscreen-res">
						<li><a href="signin.php"> Sign In </a></li>
						<li>&nbsp;<a href=""> | </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
				<?php } ?>
					<a href="ad-post-details.php" class="btn btn-post">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<!--mobile screen nav-right start-->
	<div class="nav-second">
	<?php if($_SESSION){?>		
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
			<li class="border-right"><a href="contact-us.php"> <img class="user-icon" src="images/icon/icon-call.png"/> </a></li>
			<li class="border-right"><a href="my-profile.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
			<li><a href="logout.php"> <img class="user-icon" src="images/icon/icon-logout.png"/> </a></li>
		</ul>
	<?php }else{ ?>
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
 		    <li class="border-right"><a href="contact-us.php"> <img class="user-icon" src="images/icon/icon-call.png"/> </a></li>
			<li> <a href="signin.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
		</ul>
	<?php } ?>
	</div>
	<!--mobile screen nav-right end-->

	<!-- main -->
	<section id="main" class="clearfix ad-details-page">
		<div class="container">
			<div class="adpost-details">
				<div class="row">	
					<div class="col-md-8">
						<form name="post-ad" action="DB-HANDLE/postAD.php" METHOD="POST" enctype="multipart/form-data">
							<fieldset>
								<style type="text/css">
									.postdetails {
										/*background-color: #e7edee;*/
									}

									.add-category {
										/*background-color: #e7edee;*/
									}

									.adpost-details {
										background-color: #ffffff;

									}

									.form-group {
										/*padding: 0px 20px 0px 0px;*/

									}
								</style>
								<div class="section postdetails">
									<div class="row form-group add-category">
									    <div class="form-group col-md-12">
									    	<h4><span>Fill below form to post your Ad. All fields mentioned with &#42; mark are Required!</span></h4>
									    </div>

									    <!-- <ul class="nav nav-tabs nav-justified">
										  <li class="active navbar-li-header"><a data-toggle="tab" class="navbar-a-header" onclick="check_ad_details();" href="#home">Genaral Details</a></li>
										  <li><a data-toggle="tab" onclick="check_ad_details();" href="#advertiser">Contact Details</a></li>
										  <li><a data-toggle="tab" onclick="check_ad_details();" href="#menu2">Package Details</a></li>
										  <li><a data-toggle="tab" onclick="check_ad_details();" href="#menu3">Submit your Ad!</a></li>
										</ul> -->
										
									    <div class="tab-content">
										    <div id="home" class="tab-pane fade in active">
										    	<div class="section advertisement details">
										        <h3 class="post-ad-h3">Advertisement Details</h3>

										        <div id="adtype-div" class="row form-group add-sub-category">
													<label class="col-sm-3 label-adtype">Select Ad Type<span class="required">*</span></label>
													<div class="col-sm-9">
														<select name="adtype" id="adtype" class="form-control" required="required">
															<option value="0">Select your Ad Type</option>
														</select>
													</div>
													<div id="ad-type" class="error-div col-sm-9 col-sm-offset-3" hidden="hidden">
														<span>Please Select your Ad Type!</span>
													</div>
												</div>

												<div id="adlocation-div" class="row form-group add-sub-category">
													<label class="col-sm-3 label-adlocation">Select your Region<span class="required">*</span></label>
													<div class="col-sm-9">
														<select name="adlocation" id="adlocation" class="form-control" required="required">
															<option value="-1">Select your Region</option>
														</select>
													</div>
													<div id="ad-region" class="error-div col-sm-9 col-sm-offset-3" hidden="hidden">
														<span>Please Select your Region!</span>
													</div>
												</div>

												<div class="row form-group add-title">
													<label class="col-sm-3 label-title">ad's Title<span class="required">*</span></label>
													<div class="col-sm-9">
														<input type="text" name="AdTitle" id="AdTitle" class="form-control" placeholder="Enter your Ad title" required="required" maxlength="35">
													</div>
													<div id="ad-tittle" class="error-div col-sm-9 col-sm-offset-3" hidden="hidden">
														Please Enter Advertisement Tittle!
													</div>
												</div>
												
												<div class="row form-group add-image">
													<label class="col-sm-3 label-title">Photos of your ad</label>
													<div class="col-sm-9">
														<div class="upload-section">
															<label class="upload-image col-sm-1" for="upload-image-one">
																<input type="file" name="files[]" multiple id="upload-image-one">
															</label>
															<h5 class="col-md-9"><i class="fa fa-exclamation-triangle notice-red" aria-hidden="true"></i> click &nbsp;<i class="fa fa-picture-o" aria-hidden="true"></i>&nbsp; icon on left side to add photos<span>More images able to attract more Customers</span><span>Upto 5 Photos for free</span></h5>
														</div>
													</div>
												</div>
												
												<div class="row form-group">
													<label class="col-sm-3 label-title">Price</label>
													<div class="col-sm-5">
														<input type="text" class="form-control" name="Price" id="Price" placeholder="Rs" defaultvalue="0"/>
													</div>
													<div class="col-sm-4">
														<select id="pricetype" name="pricetype" class="form-control">
															<!-- <option value="0">Select Price type</option> -->
														</select>
													</div>
												</div>
												<div class="row form-group">
													<label class="col-sm-3 label-title">Address (optional)</label>
													<div class="col-sm-9">
														<textarea class="form-control" id="official_address" name="official_address" placeholder="Enter your Official Address here (Optional)" rows="5"></textarea>
													</div>
												</div>
												
												<div class="row form-group item-description">
													<label class="col-sm-3 label-title">Description<span class="required">*</span></label>
													<div class="col-sm-9">
														<textarea onkeyup="countChar(this)" class="form-control" id="AdDesc" name="AdDesc" placeholder="Write few lines about your products" rows="8" required="required"></textarea>		
													</div>
													<div id="ad-description" class="error-div col-sm-9 col-sm-offset-3" hidden="hidden">
														<span>More descriptions to attract more customers!</span>
													</div>
												</div>
												<div class="row">
													<div class="col-sm-9 col-md-6 col-sm-offset-3">
														<p><span id="charNum">5000</span> Charactors Left</p>
													</div>
												</div>
												<div class="row form-group add-title">
													<label class="col-sm-3 label-sales-code">Sales Code</label>
													<div class="col-sm-4">
														<input type="text" name="salescode" id="salescode" class="form-control" placeholder="Enter your Sales Code" maxlength="4">
													</div>
													<div class="col-sm-5">
														<h5 class="label-sales-code">[ Please Enter If Available ]</h5>
													</div>
												</div>
											</div>
											
											<style type="text/css">
												.form-button button {
													float: right;
													margin-right: 5px;
												}
											</style>

											<div class="row">
												<div class="col-sm-12 col-md-9 col-sm-offset-3 form-button col-xs-11">
													<a href="index.php">
														<button class="btn btn-cancel">CANCEL</button>
													</a>
													<a id="next1" data-toggle="tab" href="#advertiser">
														<button class="btn btn-next" id="button1" onclick="check_ad_details();">PROCEED TO NEXT</button>
													</a>
												</div>
											</div>
										</div>

									    <div id="advertiser" class="tab-pane fade">
									    	<div class="section advertiser details">
									        <h3 class="post-ad-h3">Advertiser Details</h3>

									        <?php if($_SESSION){?>
											<div class="row form-group add-location">
												<label class="col-sm-3 label-location">Your Name<span class="required">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="fname" id="fname" class="form-control" value="<?php echo $fname ?>" placeholder="First Name" required="required">
												</div>
												
												<div class="col-sm-5">
													<input type="text" name="lname" id="lname" class="form-control" value="<?php echo $lname ?>" placeholder="Last Name" required="required">
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Your Email Address</label>
												<div class="col-sm-9">
													<input type="email" id="clientEmail" name="clientEmail" class="form-control" value="<?php echo $email ?>" placeholder="ex, jhondoe@mail.com">
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Your web Address</label>
												<div class="col-sm-9">
													<input type="text" id="clientWeb" name="clientWeb" class="form-control" value="<?php echo $web ?>" placeholder="www.example.com">
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Mobile Number<span class="required">*</span></label>
												
												<div class="col-sm-9">
													<input type="text" name="mobNo" id="mobNo" class="form-control" value="<?php echo $mob_no ?>" placeholder="Mobile Number" required="required">
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Telephone Number</label>
												
												<div class="col-sm-9">
													<input type="text" name="telNo" id="telNo" class="form-control" value="<?php echo $tel_no ?>" placeholder="Telephone Number">
												</div>
											</div>

											<?php }else{ ?>
											
											<div class="row form-group add-location">
												<label class="col-sm-3 label-location">Your Name<span class="required">*</span></label>
												<div class="col-sm-4">
													<input type="text" name="fname" id="fname" class="form-control" placeholder="First Name" required="required">
												</div>
												
												<div class="col-sm-5">
													<input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name" required="required">
												</div>

												<div class="col-sm-4 col-sm-offset-3">
													<div id="ad-description" class="error-div">
														<span>Enter your First Name!</span>
													</div>
												</div>
												
												<div class="col-sm-5">
													<div id="ad-description" class="error-div">
														<span>Enter your Last Name!</span>
													</div>
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Your Email Address<span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="email" id="clientEmail" name="clientEmail" class="form-control" placeholder="ex, jhondoe@mail.com" required="required">
												</div>
												<div class="col-sm-9 col-sm-offset-3">
													<div id="ad-description" class="error-div">
														<span>Please Enter your Email Address!</span>
													</div>
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Your web Address</label>
												<div class="col-sm-9">
													<input type="text" id="clientWeb" name="clientWeb" class="form-control" placeholder="www.example.com">
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Mobile Number<span class="required">*</span></label>
												
												<div class="col-sm-9">
													<input type="text" name="mobNo" id="mobNo" class="form-control" placeholder="Mobile Number" required="required">
												</div>
												<div class="col-sm-9 col-sm-offset-3">
													<div id="ad-description" class="error-div">
														<span>Even Enter your Mobile Number!</span>
													</div>
												</div>
											</div>

											<div class="row form-group">
												<label class="col-sm-3 label-title">Telephone Number</label>
												
												<div class="col-sm-9">
													<input type="text" name="telNo" id="telNo" class="form-control" placeholder="Telephone Number">
												</div>
											</div>
											<?php } ?>
											</div>

											<div class="row">
												<div class="col-sm-12 col-md-9 col-sm-offset-3 form-button col-xs-11">
													<a data-toggle="tab" href="#home">
														<button class="btn btn-cancel">CANCEL</button>
													</a>
													<a id="next1" data-toggle="tab" href="#menu2">
														<button class="btn btn-next" id="button2" onclick="check_ad_details();">PROCEED TO NEXT</button>
													</a>
												</div>
											</div>
									    </div>


								<div id="menu2" class="tab-pane fade">
							       	<div class="section select-package">
								        <h3 class="post-ad-h3">Available Packages</h3>
									        
										<p>Top classed packages means more interested buyers. With lots of interested buyers, you have a better chance of selling for the price that you want. Learn more about <a href="pricing.php" target="_blank">package details.</a></p>
										<div class="col-md-6">
										<h4 id="package" class="package">Packages offered</h4>
											<ul class="premium-options" required="required">
										<?php
											$sql = "SELECT * FROM ad_pakage where status=1";
											$result = $connection->query($sql);

											if ($result->num_rows > 0) {
											    // output data of each row
											    while($row = $result->fetch_assoc()) {
											    	?>
											    	<li class="premium">
														<input type="radio" onclick="showFeatures(<?php echo $row['id'];?>);" name="premiumOption[]" value="<?php echo $row['id'];?>"  id="AdPrice-<?php echo $row['id']?>">
														<label for="AdPrice-<?php echo $row['id']?>"><?php echo $row['name']?></label>
														<span>Rs <?php echo number_format($row['price'],2);?></span>
														
													</li>
											    	<?php
											        
											    }
											} else {
											    echo "0 results";
											}
										?>
											
											
											
										</ul>
										</div>
										<style type="text/css">
											#package {
												color: #999999;
											    margin-top: 25px;
											    font-size: 20px;
											    text-align: center;
											}
										</style>
										<div class="col-md-6 package-features">
										<h4 id="package" class="package">Package Includes</h4>
										<!-- ++++++++++++++++++++++++++++++ -->
										<div class="container">

											<div class="pricing-section">
												<div class="row">
													<div class="col-sm-7">
														<div class="pric">													
															
															<div class="pric-menu">
																<ul id="feature_div">
																	<div class="col-md-12">
																		<div id="ad-description" class="error-div">
																			
																		</div>
																	</div>
																</ul>
																
															</div>
														</div>
													</div>
											
													

												</div><!-- row -->
											</div><!-- pricing section -->
										</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-12 col-md-9 col-sm-offset-3 form-button col-xs-11">
											<a data-toggle="tab" href="#advertiser">
												<button class="btn btn-cancel">CANCEL</button>
											</a>
											<a id="next1" data-toggle="tab" href="#menu3">
												<button class="btn btn-next" id="button3" onclick="check_ad_details();">PROCEED TO NEXT</button>
											</a>
										</div>
									</div>
								</div>
									      <div id="menu3" class="tab-pane fade">
									        	<div class="checkbox section social media">
										<h3 class="post-ad-h3">Additional Features</h3>
										<?php
											$sql = "SELECT * FROM additional_feature where status=1";
											$result = $connection->query($sql);

											if ($result->num_rows > 0) {
											    // output data of each row
											    while($row = $result->fetch_assoc()) {
											    	?>
											    	<label>
														<input type="checkbox" value="<?php echo $row['id']; ?>" name="adtional_fe[]" >
														<?php echo $row['name'];?>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="icofont icofont-question-circle" data-toggle="tooltip" data-placement="left" title="<?php echo $row['note'];?>" style="font-size:24px; opacity:0.5;"></i>
													</label>
											    	<?php
											        
											    }
											} else {
											    echo "0 results";
											}
										?>
											
										
										</div>
									
												<div class="checkbox section agreement">
													<h3>User Agreement & Privacy Policy</h3>
													<label for="agreement">
														<input type="checkbox" name="agreement" id="agreement" required="required">
														By clicking "Post" you agree to our <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a> also acknowledge that you are the rightful owner of this item and using Trade to find a genuine buyer.
													</label>
												</div>
											<div class="row">
												<div class="col-sm-12 col-md-9 col-sm-offset-3 form-button col-xs-11">
													<a data-toggle="tab" href="#menu2">
														<button class="btn btn-cancel">CANCEL</button>
													</a>
													
													<button class="btn btn-next" onclick="check_ad_details();">SUBMIT TO REVIEW</button>
													
												</div>
											</div>
									    	</div>
									    </div>
									  </div>
								</div>
							</fieldset>
						</form>	
					</div>

					<!-- quick-rules -->	
					<div class="col-md-4 guide-note">
						<div class="section quick-rules" style="color: #7f7f7f; font-size: 13px; margin-top: 10px;">
							<div class="faq-page">
								<div class="accordion">
									<div class="panel-group" id="accordion">			
										<div class="panel panel-default panel-faq adpost-panel">
											<div class="panel-heading active-faq">
												<a data-toggle="collapse" data-parent="#accordion" href="#faq-one">
													<h3 class="panel-title">
													Advertisements posting Guide
													<span class="pull-right"><i class="fa fa-minus"></i></span>
													</h3>
												</a>
											</div><!-- panel-heading -->
											<div id="faq-one" class="panel-collapse collapse collapse in">
												<div class="panel-body">
													<ul>
														<li class="">Make sure you post in the correct category.</li>
														<li class="">Do not post the same ad more than once or repost an ad within 48 hours.</li>
														<li>Do not upload pictures with watermarks.</li>
														<li class="">Do not post ads containing multiple items unless it's a package deal.</li>
														<li class="">Do not put your email or phone numbers in the title or description.</li>
													</ul>
												</div>
											</div>
										</div><!-- panel -->
										<div class="panel panel-default panel-faq adpost-panel">
											<div class="panel-heading active-faq">
												<a data-toggle="collapse" data-parent="#accordion" href="#faq-two">
													<h3 class="panel-title">
													Payment methods for advertising
													<span class="pull-right"><i class="fa fa-plus"></i></span>
													</h3>
												</a>
											</div><!-- panel-heading -->
											<div id="faq-two" class="panel-collapse collapse collapse out">
												<div class="panel-body">
													<ul>
														<li class="">Post your Advertisement by filling this form and submit.</li>
														<li class="">Within 2 hours our sales team will contact you for further clarification.</li>
														<li class="">After We will send you a SMS with our Official Bank Account Number, A email Address & A verification code. Diposit Advertising payment to the bank account and send us a copy of the payment receipt with the verification code via viber, whatsapp or email.</li>
														<li class="">After payment is confirmed, your advertisement will be online within 1 working hour.</li>
													</ul>
												</div>
											</div>
										</div><!-- panel -->
									</div>
								</div>
							</div><!-- faq-page -->
						</div>
					</div><!-- quick-rules-->	
				</div><!-- photos-ad -->				
			</div>	
		</div><!-- container -->
	</section><!-- main -->
	
	<!-- footer -->
	<footer id="footer" class="clearfix">
		

		
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->

		<!--Start of tawk.to Script-->
		<!-- <script type="text/javascript">
		var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
		(function(){
		var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
		s1.async=true;
		s1.src='https://embed.tawk.to/58cbb0a4ab48ef44ecdaa825/default';
		s1.charset='UTF-8';
		s1.setAttribute('crossorigin','*');
		s0.parentNode.insertBefore(s1,s0);
		})();
		</script> -->
		<!--End of Tawk.to Script-->
	
	</footer><!-- footer -->

	<!--/Preset Style Chooser--> 
	<!-- <div class="style-chooser">
		<div class="style-chooser-inner">
			<a href="#" class="toggler"><i class="fa fa-life-ring fa-spin"></i></a>
			<h4>Presets</h4>
			<ul class="preset-list clearfix">
				<li class="preset1 active" data-preset="1"><a href="#" data-color="preset1"></a></li>
				<li class="preset2" data-preset="2"><a href="#" data-color="preset2"></a></li>
				<li class="preset3" data-preset="3"><a href="#" data-color="preset3"></a></li>        
				<li class="preset4" data-preset="4"><a href="#" data-color="preset4"></a></li>
			</ul>
		</div>
	</div> -->
	<!--/End:Preset Style Chooser-->
	
    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.countdown.js"></script>    
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
	
	  <!--  -->
  <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
  <link href="BOOSTRAP-FILEINPUT/css/fileinput.css" media="all" rel="stylesheet" type="text/css" /> -->
  <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> -->
  <!-- <script src="BOOSTRAP-FILEINPUT/js/fileinput.js" type="text/javascript"></script>
  <script src="BOOSTRAP-FILEINPUT/js/fileinput_locale_fr.js" type="text/javascript"></script>
  <script src="BOOSTRAP-FILEINPUT/js/fileinput_locale_es.js" type="text/javascript"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script> -->
  <!--  -->

	  <script>
  $("#file-1").fileinput({
      showUpload: false,
      uploadAsync: false,
      uploadUrl: '#', // you must set a valid URL here else you will get an error
      allowedFileExtensions : ['jpg','png'],
      overwriteInitial: false,
      maxFileSize: 1000,
      maxFileCount:10,
      //allowedFileTypes: ['image', 'video', 'flash'],
      slugCallback: function(filename) {
          return filename.replace('(', '_').replace(']', '_');
      }
   });

  </script>

  <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

	<script type="text/javascript">
		$(document).ready(function(){  
      $.ajax({ url: "get_country.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#country").append("<option value="+x[i]['id']+">"+x[i]['nicename']+"</option>") ;
               };
          }
      });

});


	$(document).scroll(function(){

        var scroll_pos = $(window).scrollTop()
    if(scroll_pos > 10){

        $("#button1").click( function() {
   			
   			$(window).scrollTop(0);

 		});

    }
    });

    $(document).scroll(function(){

        var scroll_pos = $(window).scrollTop()
    if(scroll_pos > 10){

        $("#button2").click( function() {
   			
   			$(window).scrollTop(0);

 		});

    }
    });

    $(document).scroll(function(){

        var scroll_pos = $(window).scrollTop()
    if(scroll_pos > 10){

        $("#button3").click( function() {
   			
   			$(window).scrollTop(0);

 		});

    }
    });



		$(document).ready(function(){  
      $.ajax({ url: "get_country.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#mobNoCountry").append("<option value="+x[i]['phonecode']+">+"+x[i]['phonecode']+"</option>") ;
               };
          }
      });

});

		$(document).ready(function(){  
      $.ajax({ url: "get_country.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#telNoCountry").append("<option value="+x[i]['phonecode']+">+"+x[i]['phonecode']+"</option>") ;
               };
          }
      });

});

		$(document).ready(function(){  
      $.ajax({ url: "get_category.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#CatMain").append("<option value="+x[i]['id']+">"+x[i]['name']+"</option>") ;
               };
          }
      });

});

$(document).ready(function(){  
		      $.ajax({ url: "get_ad_type.php",
		          type: 'post',
		          success: function(data) {
		               var x = JSON.parse(data);
		               // console.log(x['name']);
		               for (var i = 0; i < x.length; i++) {
		                $("#adtype").append("<option value="+x[i]['id']+">"+x[i]['name']+"</option>") ;
		               };
		          }
		      });

		});

$(document).ready(function(){  
		      $.ajax({ url: "get_province.php",
		          type: 'post',
		          success: function(data) {
		               var x = JSON.parse(data);
		               // console.log(x['name']);
		               for (var i = 0; i < x.length; i++) {
		                $("#adlocation").append("<option value="+x[i]['did']+">"+x[i]['dname']+"</option>") ;
		               };
		          }
		      });

		});

$(document).ready(function(){  
		      $.ajax({ url: "get_price_type.php",
		          type: 'post',
		          success: function(data) {
		               var x = JSON.parse(data);
		               // console.log(x['name']);
		               for (var i = 0; i < x.length; i++) {
		                $("#pricetype").append("<option value="+x[i]['id']+">"+x[i]['name']+"</option>") ;
		               };
		          }
		      });

		});

function load_parent(){
  $("#CatSub").empty();
  var cat_id = $('#CatMain').val();
   $.ajax({ url: "get_sub.php",
           data: {"cat_id":cat_id},
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               if(x == ''){
                  $('#CatSub_div').hide();
               }else{
                  $("#CatSub").append("<option value='0'>Select your sub Category</option>") ;
               for (var i = 0; i < x.length; i++) {
                $("#CatSub").append("<option value="+x[i]['id']+">"+x[i]['name']+"</option>") ;
               };
               }
          }

      });
    $('#CatSub_div').show();
}
function showFeatures(id){
	  $.ajax({ url: "DB-HANDLE/get_pakage_features.php",
           data: {"pakg_id":id},
          type: 'post',
          success: function(data) {
          	  var x = JSON.parse(data);
          	   $('#feature_div').empty();
          	  $.each(x, function( index, value ) {
				 $('#feature_div').append(value);
				});
              
          }

      });
}

function toggle(checkboxID, toggleID) {
	var checkbox = document.getElementById(checkboxID);
	var toggle = document.getElementById(toggleID);
	updateToggle = checkbox.checked ? toggle.disabled=true : toggle.disabled=false;
}

function check_ad_details() {
	var adtype = document.forms["post-ad"]["adtype"].value;
	var region = document.forms["post-ad"]["adlocation"].value;
	var adtittle = document.forms["post-ad"]["AdTitle"].value;
    var description = document.forms["post-ad"]["AdDesc"].value;
    
    if (adtype == null || adtype == "0") {
    	$( "#ad-type" ).show();
    	return false;
    }
    else {
    	$( "#ad-type" ).hide();
    }

    if (region == null || region == "-1") {
    	$( "#ad-region" ).show();
    	return false;
    }
    else {
    	$( "#ad-region" ).hide();
    }

    if (adtittle == null || adtittle == "") {
    	$( "#ad-tittle" ).show();
    	return false;
    }
    else {
    	$( "#ad-tittle" ).hide();
    }

    if (description == null || description == "") {
        $( "#ad-description" ).show();
        return false;
    }
    else {
    	$( "#ad-description" ).hide();
    }



    next1
}
</script>
