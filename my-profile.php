<?php
session_start();
include 'connection.php';
 require_once 'LastLog.php';
 require_once 'Advertisement.php';
 $myAds=new Advertisement();
 $lastLoginDetails=new LastLog();
?>
<?php
if ($_SESSION) {
    $sql = "select * From user where id='" . $_SESSION['user_id'] . "'";
    $result = mysqli_query($connection, $sql);
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $fname = $row['first_name'];
            $lname = $row['last_name'];
            $email = $row['email'];
            $mob = $row['mobile_number'];
            $tel = $row['tel_no'];
            $district=$row['area_id'];
        }
    }
    $cmd="select * From district where did='".$district."'" ;
    $result2 = mysqli_query($connection,$cmd);
    while ($row=mysqli_fetch_array($result2)){
        $DistrictName=$row['dname'];
    }
}
?>
<?php
include 'connection.php';

$query = "SELECT
	ct.*,
	IFNULL(tmp1.add_count,0) as count
from post_ad ct
left join (
	select
		ad_id,
		count(id) as add_count
	from post_ad WHERE status=1 group by ad_id
)tmp1 on tmp1.ad_id = ct.id where ct.level=1 order by ct.id asc";

$result = $connection->query($query);
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <style type="text/css">
            .registrationFormAlert{
                color: red;
            }
        </style>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="Theme Region">
        <meta name="description" content="">

        <title><?php echo $fname ?> <?php echo $lname ?> | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

        <!-- CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/icofont.css">
        <link rel="stylesheet" href="css/owl.carousel.css">
        <link rel="stylesheet" href="css/slidr.css">
        <link rel="stylesheet" href="css/main.css">
        <link id="preset" rel="stylesheet" href="css/presets/preset1.css">
        <link rel="stylesheet" href="css/responsive.css">

        <!-- font -->
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

        <!-- icons -->
        <link rel="icon" href="images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
        <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
        <!-- icons -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Template Developed By ThemeRegion -->
    </head>
    <body>
        <!-- header -->
        <header id="header" class="clearfix">
            <!-- navbar -->
            <nav class="navbar navbar-default">
                <div class="container">
                    <!-- navbar-header -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
                    </div>
                    <!-- /navbar-header -->

                    <div class="navbar-left">
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a>
                                <!-- <ul class="dropdown-menu">
                                    <li class="active"><a href="index-2.php">Home Default </a></li>
                                    <li><a href="index-one.php">Home Page V-1</a></li>
                                    <li><a href="index-two.php">Home Page V-2</a></li>
                                    <li><a href="index-three.php">Home Page V-3</a></li>
                                    <li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
                                    <li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
                                </ul> -->
                            </li>
                            <!-- <li><a href="index-one.php">Category</a></li> -->
                            <li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
                            <li><a href="faq.php">Support</a></li> 
                            <!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="ad-post.php">Ad post</a></li>
                                    <li><a href="details.php">Ad Details</a></li>
                                    <li><a href="my-ads.php">My Ads</a></li>
                                    <li><a href="my-profile.php">My Profile</a></li>
                                    <li><a href="favourite-ads.php">Favourite Ads</a></li>
                                    <li><a href="archived-ads.php">Archived Ads</a></li>
                                    <li><a href="pending-ads.php">Pending Ads</a></li>
                                    <li><a href="delete-account.php">Close Account</a></li>
                                    <li><a href="published.php">Ad Publised</a></li>
                                    <li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
                                    <!-- <li><a href="pricing.php">Pricing</a></li> -->
                                    <!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
                                    <li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
                                </ul>
                            </li> -->
                            <li><a href="about-us.php">ABout Us</a></li>
                            <li><a href="contact-us.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                
                <!-- nav-right -->
                <div class="nav-right">
                <?php if($_SESSION){?>      
                    <ul class="sign-in noscreen-res">
                        <li>
                            <dropdown class="dropdown-toggle" data-toggle="dropdown"><a href="#"><?php echo $fname ?>&nbsp;<span class="caret"></span></a></dropdown><font style="color: #ffffff; font-weight: normal;">&nbsp;&nbsp;|&nbsp;&nbsp;<a href="logout.php">Log Out</a></font>
                            <ul class="dropdown-menu">
                              <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-ads.php">My Ads</a></li>
                              <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">My Profile</a></li>
                              <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">&nbsp;</a></li>
                            </ul>
                        </li>
                    </ul>
                <?php }else{ ?>
                    <ul class="sign-in noscreen-res">
                        <li><a href="signin.php"> Sign In </a></li>
                        <li>&nbsp;<a href=""> | </a></li>
                        <li><a href="signup.php">Register</a></li>
                    </ul>
                <?php } ?>
                    <a href="ad-post-details.php" class="btn btn-post">Post Your Ad!</a>
                </div>
                <!-- nav-right -->
            </div><!-- container -->
        </nav><!-- navbar -->
    </header><!-- header -->

    <!--mobile screen nav-right start-->
    <div class="nav-second">
    <?php if($_SESSION){?>      
        <ul class="sign-in noscreenmin">
            <li class="pull-left ads"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-tags"></i> All Ads </a></li>
            <li><a href="my-profile.php"> <?php echo $fname ?> </a></li>
            <li>&nbsp;<a href=""> | </a></li>
            <li><a href="logout.php">Log out</a></li>
        </ul>
    <?php }else{ ?>
        <ul class="sign-in noscreenmin">
            <li class="pull-left ads"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
            <li><a href="signin.php"> Sign In </a></li>
            <li>&nbsp;<a href=""> | </a></li>
            <li><a href="signup.php">Register</a></li>
        </ul>
    <?php } ?>
    </div>
    <!--mobile screen nav-right end-->

        <!-- ad-profile-page -->
        <section id="main" class="clearfix  ad-profile-page">
            <div class="container">

                <div class="ad-profile section">
                    <!-- user-profile -->
                    <div class="user-profile">
                        <!-- <div class="user-images">
                        </div> -->
                        <div class="user">
                            <h2>Hello, <a href="#"><?php echo $fname ?> <?php echo $lname ?></a></h2>
                            <h5>You last logged in at: <?php echo $lastLoginDetails->getLoginDetails()?></h5>
                        </div>

                        <div class="favorites-user">
                            <div class="my-ads">
                                <a href="my-ads.php"><?php echo $row['count']; ?><small>My ADS</small><?php echo $myAds->getMyAddCount($_SESSION['user_id']) ?></a>
                            </div>
                            <!-- <div class="favorites">
                                    <a href="#">00<small>Favorites</small></a>
                            </div> -->
                        </div>
                    </div><!-- user-profile -->
                    <ul class="user-menu">
                        <li class="active"><a href="my-profile.php">Profile</a></li>
                        <li><a href="my-ads.php">Published ads</a></li>
                        <!-- <li><a href="favourite-ads.php">Favourite ads</a></li>
                        <li><a href="archived-ads.php">Archived ads </a></li> -->
                        <li><a href="pending-ads.php">Pending approval</a></li>
                        <li><a href="unpublished-ads.php">Deactivated Ads </a></li>
                        <li><a href="delete-account.php">Close account</a></li>
                    </ul>
                </div><!-- ad-profile -->

                <div class="profile section">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="user-pro-section">
                                <!-- profile-details -->
                                <div class="profile-details section">
                                    <h2>Profile Details</h2>
                                    <!-- form -->

                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input type="text" class="form-control col-md-6" name="" value="<?php echo $fname ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control col-md-6" value="<?php echo $lname ?>">
                                    </div>

                                    <div class="form-group">
                                        <label>Email Address</label>
                                        <input type="email" class="form-control" value="<?php echo $email ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="name-three">Mobile Number</label>
                                        <input type="text" class="form-control" value="<?php echo $mob ?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="name-three">Telephone Number</label>
                                        <input type="text" class="form-control" value="<?php echo $tel ?>">
                                    </div>
                                    <div class="form-group">
                                    <label>Your City</label>
                                    <select name="dname" class="form-control">
                                        <option selected><?php echo $DistrictName ?></option>
                                        <?php $sql = mysqli_query($connection,"SELECT * FROM district ORDER BY dname ASC"); while ($row = mysqli_fetch_array($sql)) { ?>
                                            <option><?php echo $row['dname']?></option>
                                        <?php } ?>

                                    </select>
                                </div>

                                    <div class="form-group">
                                            <label>You are a</label>
                                            <select class="form-control">
                                                    <option value="#">Dealer</option>
                                                    <option value="#">Individual Seller</option>
                                            </select>
                                    </div>

                                    <button type="submit" href="#" id = "updateButton" class="btn btn-success">Update Profile</button>
                                    <button type="reset" href="#" class="btn btn-danger" onclick="bClick()">Cancel</button>

                                </div><!-- profile-details -->

                                <!-- change-password -->
                                <form action="my-profile.php" method="Post" name="user_profile">
                                    <div class="change-password section">
                                        <h2>Change password</h2>
                                        <!-- form -->
                                        <div class="form-group ">
                                            <label>Old Password</label>
                                            <input type="password" name="txtOldPassword" id="txtOldPassword" class="form-control" onkeyup="buttonDisable();">
                                        </div>

                                        <div class="registrationFormAlert" id="divCheckPassword">
                                        </div>
                                        <div class="form-group">
                                            <label>New password</label>
                                            <input type="password" name="txtNewPassword" id="txtNewPassword" class="form-control" onkeyup="buttonDisable();">
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm password</label>
                                            <input type="password" name="txtConfirmPassword" id="txtConfirmPassword" class="form-control" onkeyup ="checkPasswordMatch();">
                                        </div>

                                        <div class="registrationFormAlert" id="divCheckPasswordMatch">
                                        </div>
                                        <script type="text/javascript">
                                        	function buttonDisable() {
                                        		document.getElementById("updateButton").disabled = true;
                                        	}
                                        </script>

                                        <script type="text/javascript">
                                            function checkPasswordMatch() {
                                            	var oldPassword = $("#txtOldPassword").val();
                                                var password = $("#txtNewPassword").val();
                                                var confirmPassword = $("#txtConfirmPassword").val();
                                                if(oldPassword != "" && password != "" && confirmPassword != ""){
                                                	if (password != confirmPassword){
                                                    	$("#divCheckPasswordMatch").html("The Password Confirmation field does not match the New Password field.");
                                                	}
                                               		else{
                                                    	$("#divCheckPasswordMatch").html("");
                                                    	document.getElementById("updateButton").disabled = false;
                                               		}

                                                }
                                                else
                                                  	$("#divCheckPasswordMatch").html("Fill all Details correctly.");
                                            }

                                            $(document).ready(function () {
                                                $("#txtNewPassword, #txtConfirmPassword").keyup(checkPasswordMatch);
                                            });
                                        </script>

                                    </div><!-- change-password -->

                                    <!-- preferences-settings -->
                                    <div class="preferences-settings section">
                                        <h2>Other Settings</h2>
                                        <!-- checkbox -->
                                        <div class="checkbox">
                                                <!-- <label><input type="checkbox" name="logged"> Comments are enabled on my ads </label> -->
                                            <label><input type="checkbox" name="receive"> I want to receive newsletter.</label>
                                            <!-- <label><input type="checkbox" name="want">I want to receive advice on buying and selling. </label> -->
                                        </div><!-- checkbox -->
                                    </div><!-- preferences-settings -->
                                    <button type="submit" href="#" id = "updateButton" class="btn btn-success">Update Profile</button>
                                    <button type="reset" href="#" class="btn btn-danger" onclick="bClick()">Cancel</button>
                                </form>
                            </div><!-- user-pro-edit -->
                        </div><!-- profile -->

                        <?php
                        include 'connection.php';
                        ?>

                        <?php
                        $password;
                        if ($_SESSION) {
                            $sql = "select * From user where id='" . $_SESSION['user_id'] . "'";
                            $result = mysqli_query($connection, $sql);
                            if (mysqli_num_rows($result) > 0) {
                                while ($row = mysqli_fetch_assoc($result)) {
                                    $password = $row['password'];
                                }
                            }
                        }

                        if (isset($_POST['txtOldPassword'])and$_POST['txtConfirmPassword']) {
                        	$pword = $_POST['txtOldPassword'];
                            $npword = $_POST['txtConfirmPassword'];
                            if (md5($pword) == $password) {
                                $sql = "Update user Set password='" . md5($npword) . "' where id='" . $_SESSION['user_id'] . "'";
                                $result = mysqli_query($connection, $sql);
                                $ok = 0;
                                	if ($result==1) {
                                		?>
                                		<script type="text/javascript">
                                		alert("Password changed successfully!");
                               			</script>
                               			<?php
                               			$ok = 1;
                                	}
                                	if($ok == 0){
                                		?>
                                		<script type="text/javascript">
                                		alert("Incorrect Old password.");
                                		document.getElementById("txtOldPassword").Focus();
                               			</script>
                               			<?php
                                	}

                            }
                        }
                        ?>
                        <div class="col-sm-4 text-center">
                            <!-- recommended-cta -->
                            <div class="recommended-cta">
                                <div class="cta">
                                    <!-- single-cta -->
                                    <div class="single-cta">
                                        <!-- cta-icon -->
                                        <div class="cta-icon icon-secure">
                                            <img src="images/icon/13.png" alt="Icon" class="img-responsive">
                                        </div><!-- cta-icon -->

                                        <h4>Secure Trading</h4>
                                    </div><!-- single-cta -->

                                    <!-- single-cta -->
                                    <div class="single-cta">
                                        <!-- cta-icon -->
                                        <div class="cta-icon icon-support">
                                            <img src="images/icon/14.png" alt="Icon" class="img-responsive">
                                        </div><!-- cta-icon -->

                                        <h4>24/7 Support</h4>
                                    </div><!-- single-cta -->


                                    <!-- single-cta -->
                                    <div class="single-cta">
                                        <!-- cta-icon -->
                                        <div class="cta-icon icon-trading">
                                            <img src="images/icon/15.png" alt="Icon" class="img-responsive">
                                        </div><!-- cta-icon -->

                                        <h4>Easy Trading</h4>
                                    </div><!-- single-cta -->

                                    <!-- single-cta -->
                                    <div class="single-cta">
                                        <h5>Need Help?</h5>
                                        <p><span>Give a call on</span><a href="tellto:+94713399099"> +94 713 399 099</a></p>
                                    </div><!-- single-cta -->
                                </div>
                            </div><!-- cta -->
                        </div><!-- recommended-cta-->
                    </div><!-- row -->
                </div>
            </div><!-- container -->
        </section><!-- ad-profile-page -->

        <!-- footer -->
        <footer id="footer" class="clearfix">
            <div class="footer-bottom clearfix text-center">
                <div class="container">
                    <p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http://www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
                </div>
            </div><!-- footer-bottom -->
        </footer><!-- footer -->

        <!-- JS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/modernizr.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="js/gmaps.min.js"></script>
        <script src="js/goMap.js"></script>
        <script src="js/map.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/smoothscroll.min.js"></script>
        <script src="js/scrollup.min.js"></script>
        <script src="js/price-range.js"></script>
        <script src="js/jquery.countdown.js"></script>
        <script src="js/custom.js"></script>
        <script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
    </body>
</html>
