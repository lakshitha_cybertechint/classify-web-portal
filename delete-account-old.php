<?php session_start();
 include 'connection.php';
 require_once 'LastLog.php';
 require_once 'Advertisement.php';
 $myAds=new Advertisement();
 $lastLoginDetails=new LastLog();
?>
<?php
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ;
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   	   $lname=$row['last_name'] ;
	   	   $email=$row['email'] ;
	   	   $mob=$row['mobile_number'] ;
	   	   $tel=$row['tel_no'] ;
	   }
	}
}
$adCount=5;
$sql="SELECT * FROM post_ad where user_id_2='".$_SESSION['user_id']."'";
$q=mysqli_query($connection,$sql);
$adCount=mysqli_num_rows($q);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Delete your account | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/slidr.css">
    <link rel="stylesheet" href="css/main.css">
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">
    <link rel="stylesheet" href="css/responsive.css">

	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->

				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php"><i class="fa fa-home" style="font-size:18px; color:red;">&nbsp;</i><strong>Home</strong></a>

							</li>
							<li><a href="index-one.php">Category</a></li>
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li>

							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>

				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>
					<ul class="sign-in">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><font color="black">Hi!</font>&nbsp;&nbsp;&nbsp;<a href="#"><?php echo $fname ?><span class="caret"></span></a></dropdown>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" href="my-ads.php">My Account</a></li>
						      <li><a class="page-scroll" href="my-profile.php">Settings</a></li>
						      <li><a class="page-scroll" href="logout.php">Logout</a></li>
						    </ul>
						</li>
					</ul>
						<?php }else{ ?>
					<ul class="sign-in">
						<li><i class="fa fa-user"></i></li>
						<li><a href="signin.php"> Sign In </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
						<?php } ?>
					<a href="ad-post-details.php" class="btn">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<!-- delete-page -->
	<section id="main" class="clearfix delete-page">
		<div class="container">

			<div class="ad-profile section">
					<!-- user-profile -->
					<div class="user-profile">
						<!-- <div class="user-images">
						</div> -->
						<div class="user">
							<h2>Hello, <a href="#"><?php echo $fname ?> <?php echo $lname ?></a></h2>
							<h5>You last logged in at: <?php echo $lastLoginDetails->getLoginDetails()?></h5>
						</div>


						<div class="favorites-user">
							<div class="my-ads">
								<a href="my-ads.php"><small>My ADS</small><?php echo $myAds->getMyAddCount($_SESSION['user_id']) ?></a>
							</div>
							<!-- <div class="favorites">
								<a href="#">00<small>Favorites</small></a>
							</div> -->
						</div>
					</div><!-- user-profile -->

					<ul class="user-menu">
						<li><a href="my-profile.php">Profile</a></li>
						<li><a href="my-ads.php">Published ads</a></li>
						<!-- <li><a href="favourite-ads.php">Favourite ads</a></li>
					<li><a href="archived-ads.php">Archived ads </a></li> -->
					<li><a href="pending-ads.php">Pending approval</a></li>
					<li><a href="unpublished-ads.php">Deactivated Ads </a></li>
					<li class="active"><a href="delete-account.php">Close account</a></li>
					</ul>

			</div><!-- ad-profile -->
<form id="DeleteForm" action="deleted.php">
			<div class="close-account">
				<div class="row">
					<div class="col-sm-8 text-center">
						<div class="delete-account section">
							<h2>Delete Your Account</h2>
							<h4>Are you sure, you want to delete your account?</h4>
							<input type="submit" class="btn btn-danger" value="Delete Account" />
							<a href="index.php" class="btn success">Cancel</a>
						</div>
					</div><!-- delete-account -->
</form>
					<div class="col-sm-4 text-center">
						<!-- recommended-cta -->
						<div class="recommended-cta">
							<div class="cta">
								<!-- single-cta -->
								<div class="single-cta">
									<!-- cta-icon -->
									<div class="cta-icon icon-secure">
										<img src="images/icon/13.png" alt="Icon" class="img-responsive">
									</div><!-- cta-icon -->

									<h4>Secure Trading</h4>
								</div><!-- single-cta -->

								<!-- single-cta -->
								<div class="single-cta">
									<!-- cta-icon -->
									<div class="cta-icon icon-support">
										<img src="images/icon/14.png" alt="Icon" class="img-responsive">
									</div><!-- cta-icon -->

									<h4>24/7 Support</h4>
								</div><!-- single-cta -->


								<!-- single-cta -->
								<div class="single-cta">
									<!-- cta-icon -->
									<div class="cta-icon icon-trading">
										<img src="images/icon/15.png" alt="Icon" class="img-responsive">
									</div><!-- cta-icon -->

									<h4>Easy Trading</h4>
								</div><!-- single-cta -->

								<!-- single-cta -->
								<div class="single-cta">
									<h5>Need Help?</h5>
									<p><span>Give a call on</span><a href="tellto:+94713399099"> +94 713 399 099</a></p>
								</div><!-- single-cta -->
							</div>
						</div><!-- cta -->
					</div><!-- recommended-cta-->
				</div><!-- row -->
			</div>
		</div><!-- container -->
	</section><!-- delete-page -->

	<!-- footer -->
	<footer id="footer" class="clearfix">
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http://www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->

     <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.countdown.js"></script>
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>
