<!DOCTYPE html>
<html lang="en">

<?php include 'connection.php';

?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Controller | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">

</head>
<body>

	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php"><i class="fa fa-home" style="font-size:18px; color:red;">&nbsp;</i><strong>Home</strong></a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<li><a href="index-one.php">Category</a></li>
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
									<li><a href="pricing.php">Pricing<!-- <span class="badge">New</span> --></a></li>
									<!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
                Admin page hit count				

					<a href="//www.hashemian.com/tools/hit-counter.php"><img alt="Powered by hashemian.com" border="1" src="//www.hashemian.com/tools/hit-counter.php?a=106297&b=www.classify.lk"></a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

    <div class="ad-check-area col-md-12">
        <div class="container col-md-12">
            <div class="ad-check-form col-md-12">
                <div class="show-id col-md-12 item margin-top-admin">
                    <span class="col-md-3"></span>
                    <select name="select_ad" id="select_ad" class="ad-check-select col-md-7">
                            <option value="0">Select a Advertisement</option>
                        </select>
                </div>
            </div>
        </div>
    </div>


        

    <?php

$query = "SELECT post_ad.id AS id, post_ad.price AS price, (select web from online_user where id=post_ad.user_id) AS web, post_ad.tittle AS ad_tittle, (select first_name from online_user where id=post_ad.user_id ) AS fname, (select last_name from online_user where id=post_ad.user_id ) AS lname, (select name from ad_pakage where ad_package_id=ad_pakage.id) AS package, post_ad.sales_code AS salescode, post_ad.address AS address, post_ad.created_at AS created_at, (select mobile from online_user where id=post_ad.user_id ) AS mob_no, (select tel from online_user where id=post_ad.user_id ) AS tel_no, (select email from online_user where id=post_ad.user_id ) AS email, post_ad.description AS description FROM `post_ad` WHERE status=0 ORDER BY created_at ASC";
$result = $connection->query($query);
?>
    
    

    


    <div class="ad-check-area col-md-12">
        <div class="container col-md-12">
            <div class="ad-check-form col-md-12">

                <?php
                    while($row= $result->fetch_assoc()) {
                ?>
                <div class="show-id col-md-12 item">
                    <span class="col-md-3">Advertisement Id</span>
                    <input type="text" class="form-design col-md-7 not-important" name="" value="<?php echo $row['id'];?>" disabled />
                </div>

                <div class="show-id col-md-12 item">
                    <span class="col-md-3">Confirmation Id</span>
                    <input type="text" class="form-design col-md-3 not-important" name="" value="<?php $hash_code=substr(md5($row['id']),0,10);
 echo $hash_code;?>" disabled />
                    <input type="text" class="form-design col-md-3 col-md-offset-1 not-important" name="" value="<?php echo $row['salescode'];?>" disabled />
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Ad Tittle</span>
                    <input type="text" class="form-design col-md-7 important" name="tittle" value="<?php echo $row['ad_tittle'];?>" />
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Address</span>
                    <textarea class="ad-check-textarea col-md-4 not-important" rows="5"><?php echo $row['address'];?></textarea>
                </div>
                
                <div class="col-md-12 item">
                    <span class="col-md-3">description</span>
                    <textarea class="ad-check-textarea col-md-7 important" rows="8"><?php echo $row['description'];?></textarea>
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Download images</span>
                    <button class="ad-check-textarea btn col-md-7 important">Download all images to edit</button>
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Price</span>
                    <input type="text" class="form-design col-md-3 not-important" name="price" value="<?php echo $row['price'];?>"/>

                    <select id="type" class="ad-check-select col-md-3 col-md-offset-1 not-important" name="type">
                        <option value="1">Fixed</option>
                        <option value="2">Negotiable</option>
                        <option value="3">Per Month</option>
                        <option value="4">Upwards</option>
                    </select>
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Select Category</span>
                    <select name="CatMain" id="CatMain" onchange="load_parent();" class="ad-check-select col-md-7 important">
                        <option value="0">Select regarding Category</option>
                    </select>
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Select sub Category</span>
                    <select name="CatSub" id="CatSub" class="ad-check-select col-md-7 important">
                        <option value="0">Select regarding sub Category</option>
                    </select>
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Region</span>
                    <select name="adlocation" id="adlocation" class="ad-check-select col-md-7 important">
                        <option value="0">Select region</option>
                    </select>
                </div>


                <div class="col-md-12 item">
                    <span class="col-md-3">Advertiser Name</span>
                    <input class="form-design col-md-3 important" type="text" name="fname-check" value="<?php echo $row['fname'];?>"><input class="form-design col-md-3 col-md-offset-1 important" type="text" name="fname-check" value="<?php echo $row['lname'];?>">
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Email Address</span>
                    <input type="text" class="form-design col-md-3 important" name="email" value="<?php echo $row['email'];?>" />
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Web Site</span>
                    <input type="text" class="form-design col-md-3 not-important" name="website" value="<?php echo $row['web'];?>" />
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Mobile Number</span>
                    <input type="text" class="form-design col-md-3 important" name="mobile" value="<?php echo $row['mob_no'];?>" />
                </div>

                <div class="col-md-12 item">
                    <span class="col-md-3">Telephone Number</span>
                    <input type="text" class="form-design col-md-3 not-important" name="telephone" value="<?php echo $row['tel_no'];?>" />
                </div>

                
                <div class="col-md-12 item">
                    <span class="col-md-3">Select Package</span>
                    <div class="col-md-4">
                        <?php
                            $sql = "SELECT * FROM ad_pakage where status=1";
                            $result = $connection->query($sql);

                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    ?>
                                    <li>
                                        <input type="radio" name="premiumOption[]" value="<?php echo $row['id'];?>"  id="AdPrice-<?php echo $row['id']?>">
                                        <label for="AdPrice-<?php echo $row['id']?>"><?php echo $row['name']?></label>
                                        <span class="pull-right">Rs<?php echo $row['price']?></span>
                                    </li>
                                    <?php
                                    
                                }
                            } else {
                                echo "0 results";
                            }
                        ?>
                    </div>
                </div>

                <div class="show-created-date col-md-12 item">
                    <span class="col-md-3">Ad posted on</span>
                    <label><?php echo $row['created_at'];?></label>
                </div>

                <div class="col-md-12 item">
                    <div class="col-sm-12 col-md-7 col-sm-offset-3 form-button">
                        <a href="">
                            <button class="btn btn-cancel">CANCEL</button>
                        </a>
                        <a id="next1" data-toggle="tab" href="#advertiser">
                            <button class="btn btn-next" onclick="check_ad_details();">PROCEED TO NEXT</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
        }
    ?>

</body>

<script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.countdown.js"></script>    
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<script type="text/javascript">
	$(document).ready(function(){
      	$.ajax({ url: "get_ad.php",
          	type: 'post',
         		success: function(data){
            	var x = JSON.parse(data);
            		for (var i = 0; i < x.length; i++) {
            			$("#select_ad").append("<option value="+x[i]['id']+">"+x[i]['tittle']+",  "+x[i]['created_at']+"</option>");
               };

          }
      });

});
</script>

    <script type="text/javascript">
        $(document).ready(function(){  
      $.ajax({ url: "get_country.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#country").append("<option value="+x[i]['id']+">"+x[i]['nicename']+"</option>") ;
               };
          }
      });

});

        $(document).ready(function(){  
      $.ajax({ url: "get_country.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#mobNoCountry").append("<option value="+x[i]['phonecode']+">+"+x[i]['phonecode']+"</option>") ;
               };
          }
      });

});

        $(document).ready(function(){  
      $.ajax({ url: "get_country.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#telNoCountry").append("<option value="+x[i]['phonecode']+">+"+x[i]['phonecode']+"</option>") ;
               };
          }
      });

});

        $(document).ready(function(){  
      $.ajax({ url: "get_category.php",
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               // console.log(x['name']);
               for (var i = 0; i < x.length; i++) {
                $("#CatMain").append("<option value="+x[i]['id']+">"+x[i]['name']+"</option>") ;
               };
          }
      });

});

$(document).ready(function(){  
              $.ajax({ url: "get_province.php",
                  type: 'post',
                  success: function(data) {
                       var x = JSON.parse(data);
                       // console.log(x['name']);
                       for (var i = 0; i < x.length; i++) {
                        $("#adlocation").append("<option value="+x[i]['did']+">"+x[i]['dname']+"</option>") ;
                       };
                  }
              });

        });

function load_parent(){
  $("#CatSub").empty();
  var cat_id = $('#CatMain').val();
   $.ajax({ url: "get_sub.php",
           data: {"cat_id":cat_id},
          type: 'post',
          success: function(data) {
               var x = JSON.parse(data);
               if(x == ''){
                  $('#CatSub_div').hide();
               }else{
                  $("#CatSub").append("<option value='0'>Select your sub Category</option>") ;
               for (var i = 0; i < x.length; i++) {
                $("#CatSub").append("<option value="+x[i]['id']+">"+x[i]['name']+"</option>") ;
               };
               }
          }

      });
    $('#CatSub_div').show();
}



    </script>

</body>