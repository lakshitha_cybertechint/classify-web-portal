<?php session_start();
 include 'connection.php';
?>
<?php 
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   	   $lname=$row['last_name'] ;
	   	   $mob_no=$row['mobile_number'] ;
	   	   $tel_no=$row['tel_no'] ;
	   	   $web=$row['web'] ;
	   	   $email=$row['email'] ;
	   }
	}
}
?>

<!DOCTYPE html>
<html lang="en">

<?php ?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Post your Ad | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>
   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
    <script src="js/CharCount.js"></script>
    <script type="text/javascript">
      function countChar(val) {
        var len = val.value.length;
        if (len >= 5000) {
          val.value = val.value.substring(0, 5000);
        } else {
          $('#charNum').text(5000 - len);
        }
      };
    </script>
</head>
<body>
  	<style type="text/css">
  		h3 {
  			margin-bottom: 50px;
  		}
  	</style>
	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<!-- <li><a href="index-one.php">Category</a></li> -->
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li><a href="faq.php">Support</a></li> 
							<!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="ad-post.php">Ad post</a></li>
									<li><a href="details.php">Ad Details</a></li>
									<li><a href="my-ads.php">My Ads</a></li>
									<li><a href="my-profile.php">My Profile</a></li>
									<li><a href="favourite-ads.php">Favourite Ads</a></li>
									<li><a href="archived-ads.php">Archived Ads</a></li>
									<li><a href="pending-ads.php">Pending Ads</a></li>
									<li><a href="delete-account.php">Close Account</a></li>
									<li><a href="published.php">Ad Publised</a></li>
									<li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
									<!-- <li><a href="pricing.php">Pricing</a></li> -->
									<!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
									<li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
								</ul>
							</li> -->
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>		
					<ul class="sign-in noscreen-res">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><a href="#"><?php echo $fname ?>&nbsp;<span class="caret"></span></a></dropdown><font style="color: #ffffff; font-weight: normal;">&nbsp;&nbsp;|&nbsp;&nbsp;<a href="logout.php">Log Out</a></font>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-ads.php">My Ads</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">My Profile</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">&nbsp;</a></li>
						    </ul>
						</li>
					</ul>
				<?php }else{ ?>
					<ul class="sign-in noscreen-res">
						<li><a href="signin.php"> Sign In </a></li>
						<li>&nbsp;<a href=""> | </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
				<?php } ?>
					<a href="ad-post-details.php" class="btn btn-post">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<!--mobile screen nav-right start-->
	<div class="nav-second">
	<?php if($_SESSION){?>		
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-tags"></i> All Ads </a></li>
			<li><a href="my-profile.php"> <?php echo $fname ?> </a></li>
			<li>&nbsp;<a href=""> | </a></li>
			<li><a href="logout.php">Log out</a></li>
		</ul>
	<?php }else{ ?>
		<ul class="sign-in noscreenmin">
			<li class="pull-left ads"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
			<li><a href="signin.php"> Sign In </a></li>
			<li>&nbsp;<a href=""> | </a></li>
			<li><a href="signup.php">Register</a></li>
		</ul>
	<?php } ?>
	</div>
	<!--mobile screen nav-right end-->

	<!-- main -->
	<!-- post-page -->
	<section id="main" class="clearfix ad-post-page">
		<div class="container">
			<!-- banner -->

			<?php if($_SESSION){?>		
					<div class="breadcrumb-section">
						<h3 class="header text-center">Welcome <?php echo $fname ?>! Let's post an ad. Select Advertisement category below</h3>
					</div>
				<?php }else{ ?>
					<div class="breadcrumb-section">
						<h3 class="header text-center">Welcome Poster! Let's post an ad. Select Advertisement category below</h3>
					</div>
				<?php } ?>
				
			<div id="ad-post">
				<div class="row category-tab">
					<div class="col-md-4 col-sm-6">
						<div class="section cat-option select-category post-option">
							<h4>Select a category</h4>
							<ul role="tablist">
								<li><a href="#cat1" aria-controls="cat1" role="tab" data-toggle="tab">
									<span class="select">
										<img src="images/icon/1.png" alt="Images" class="img-">
									</span>
									Cars & Vehicles
								</a></li>
							</ul>
						</div>
					</div>
					
					<!-- Tab panes -->
					<div class="col-md-4 col-sm-6">
						<div class="section tab-content subcategory post-option">
							<h4>Select a subcategory</h4>
							<div role="tabpanel" class="tab-pane" id="cat1">
								<ul>
									<li><a href="javascript:void(0)">Cars & Buses</a></li>
									<li><a href="javascript:void(0)">Motorbikes & Scooters</a></li>
									<li><a href="javascript:void(0)">Bicycles and Three Wheelers</a></li>
									<li><a href="javascript:void(0)">Three Wheelers</a></li>
									<li><a href="javascript:void(0)">Trucks, Vans & Buses</a></li>
									<li><a href="javascript:void(0)">Tractors & Heavy-Duty</a></li>
									<li><a href="javascript:void(0)">Auto Parts & Accessories</a></li>
								</ul>	
							</div>
							<div role="tabpanel" class="tab-pane" id="cat2">
								<ul>
									<li><a href="javascript:void(0)">Laptop & Computer</a></li>
									<li><a href="javascript:void(0)">Mobile Phones</a></li>
									<li><a href="javascript:void(0)">Phablet & Tablets</a></li>
									<li><a href="javascript:void(0)">Audio & MP</a></li>
									<li><a href="javascript:void(0)">Accessories</a></li>
									<li><a href="javascript:void(0)">Cameras</a></li>
									<li><a href="javascript:void(0)">Mobile Accessories</a></li>
									<li><a href="javascript:void(0)">TV & Video</a></li>
									<li><a href="javascript:void(0)">Other Electronics</a></li>
									<li><a href="javascript:void(0)">TV & Video Accessories</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat3">
								<ul>
									<li><a href="javascript:void(0)">Houses & Plots</a></li>
									<li><a href="javascript:void(0)">Lands & property</a></li>
									<li><a href="javascript:void(0)">Plots & Lands</a></li>
									<li><a href="javascript:void(0)">Apartment</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat4">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat5">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat6">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat7">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat8">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat9">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat10">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
							<div role="tabpanel" class="tab-pane" id="cat11">
								<ul>
									<li><a href="javascript:void(0)">Sub Category Item</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-6">
						<div class="section next-stap post-option">
							<h4>There is one more page to proceed!</h4>
							<!-- <p align="justify">Please DO NOT post multiple ads for the same items or service. All duplicate, spam and wrongly categorized ads will be deleted.</p> -->
							<div class="btn-section" align="center">
								<a class="btn btn-primary" href="ad-post-details.php" role="button">Proceed to Next!</a>
							</div>
						</div>
					</div><!-- next-stap -->
				</div>
				<!-- <div class="row">
					<div class="col-sm-8 col-sm-offset-2 text-center">
						<div class="ad-section">
							<a href="#"><img src="images/ads/3.jpg" alt="Image" class="img-responsive"></a>
						</div>
					</div>
				</div> --><!-- row -->
			</div>				
		</div><!-- container -->
	</section><!-- post-page -->
	
	<!-- footer -->
	<footer id="footer" class="clearfix">
		<!-- footer-top -->
		<!-- <section class="footer-top clearfix">
			<div class="container">
				<div class="row">
					
					<div class="col-sm-3">
						<div class="footer-widget">
							<h3>Quik Links</h3>
							<ul>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Contact Us</a></li>
								<li><a href="#">Careers</a></li>
								<li><a href="#">All Cities</a></li>
								<li><a href="#">Help & Support</a></li>
								<li><a href="#">Advertise With Us</a></li>
								<li><a href="#">Blog</a></li>
							</ul>
						</div>
					</div>
					
					<div class="col-sm-3">
						<div class="footer-widget">
							<h3>How to sell fast</h3>
							<ul>
								<li><a href="#">How to sell fast</a></li>
								<li><a href="#">Membership</a></li>
								<li><a href="#">Banner Advertising</a></li>
								<li><a href="#">Promote your ad</a></li>
								<li><a href="#">Trade Delivers</a></li>
								<li><a href="#">FAQ</a></li>
							</ul>
						</div>
					</div>

					
					<div class="col-sm-3">
						<div class="footer-widget social-widget">
							<h3>Follow us on</h3>
							<ul>
								<li><a href="#"><i class="fa fa-facebook-official"></i>Facebook</a></li>
								<li><a href="#"><i class="fa fa-twitter-square"></i>Twitter</a></li>
								<li><a href="#"><i class="fa fa-google-plus-square"></i>Google+</a></li>
								<li><a href="#"><i class="fa fa-youtube-play"></i>youtube</a></li>
							</ul>
						</div>
					</div>

					
					<div class="col-sm-3">
						<div class="footer-widget news-letter">
							<h3>Newsletter</h3>
							<p>Trade is Worldest leading classifieds platform that brings!</p>
							
							<form action="#">
								<input type="email" class="form-control" placeholder="Your email id">
								<button type="submit" class="btn btn-primary">Sign Up</button>
							</form>	
						</div>
					</div>
				</div>
			</div>
		</section> -->

		
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->
	
	<!--/Preset Style Chooser--> 
	<!-- <div class="style-chooser">
		<div class="style-chooser-inner">
			<a href="#" class="toggler"><i class="fa fa-life-ring fa-spin"></i></a>
			<h4>Presets</h4>
			<ul class="preset-list clearfix">
				<li class="preset1 active" data-preset="1"><a href="#" data-color="preset1"></a></li>
				<li class="preset2" data-preset="2"><a href="#" data-color="preset2"></a></li>
				<li class="preset3" data-preset="3"><a href="#" data-color="preset3"></a></li>        
				<li class="preset4" data-preset="4"><a href="#" data-color="preset4"></a></li>
			</ul>
		</div>
	</div> -->
	<!--/End:Preset Style Chooser-->
	
    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.countdown.js"></script>    
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>