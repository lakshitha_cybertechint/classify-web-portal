<?php session_start();
 include 'connection.php';
 include 'counter.php';

$count_q="SELECT count(*) as total from ad_visit where ad_id=".$_REQUEST['id']."";
$result_count = $connection->query($count_q);
$result_count=$result_count->fetch_assoc();

?>
<?php 
$fname="";
if($_SESSION){
    $sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
    $result = mysqli_query($connection,$sql);
    if(mysqli_num_rows($result)>0){
       while($row = mysqli_fetch_assoc($result)){
           $fname=$row['first_name'] ;
       }
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<?php

// $query_banner = "SELECT * from banner_ads WHERE status=1 AND position=details";
// $result_banner = $connection->query($query_banner);
// while($row_banner = $result_banner->fetch_assoc());

$query = "SELECT post_ad.price AS ad_price, post_ad.address AS address, (select web from online_user where id=post_ad.user_id ) AS web, (select name from category where id=post_ad.sub_category_id ) AS sub_cat, post_ad.tittle AS ad_tittle,post_ad.id AS ad_id, (select first_name from online_user where id=post_ad.user_id ) AS user_first, (select to_display from ad_type where id=post_ad.type_id ) AS ad_type, (select echo_name from price_type where id=post_ad.price_type_id ) AS price_type, post_ad.ad_premium_id AS delivery_option, post_ad.video_url AS video_url, (select last_name from online_user where id=post_ad.user_id ) AS user_last,post_ad.created_at AS cre_date, (select dname from district where did=post_ad.area_id) AS location, (select mobile from online_user where id=post_ad.user_id ) AS mob_no, (select tel from online_user where id=post_ad.user_id ) AS tel_no, (select email from online_user where id=post_ad.user_id ) AS email,post_ad.description AS description FROM `post_ad` where 
id=".$_REQUEST['id']." AND status=1";
$result = $connection->query($query);

header('Content-Type: text/html; charset=utf-8');

?>

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Classifieds Lanka (pvt) Ltd">
    <?php

        while($row = $result->fetch_assoc()) {
    ?>

    <title><?php echo $row['ad_tittle']?> on Deals | Classify.lk | Sri Lanka's Largest Classifieds web Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
    <link id="preset" rel="stylesheet" href="css/presets/preset1.css">  
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="image_src" type="image/jpeg" href="http://classify.lk/images/logo.png" />
    
    <!-- font -->
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- icons -->
    <link rel="icon" href="images/ico/favicon.ico"> 
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>

<script src="js/popUp.js"></script>

<script type="text/javascript">

    function useremail() {
            $("#popup").hide().fadeIn(1000);
    $("#newsletter_background").fadeIn(1000);

    //close the POPUP if the button with id="close" is clicked
    $("#close").on("click", function (e) {
        e.preventDefault();
        // $("#popup").fadeOut(1000);
        // $("#newsletter_background").fadeOut(1000);
    });
}

  $(document).ready(function() {
    $('#contact_form').bootstrapValidator({
        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            first_name: {
                validators: {
                        stringLength: {
                        min: 2,
                    },
                        notEmpty: {
                        message: 'Please supply your first name'
                    }
                }
            },
             last_name: {
                validators: {
                     stringLength: {
                        min: 2,
                    },
                    notEmpty: {
                        message: 'Please supply your last name'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your email address'
                    },
                    emailAddress: {
                        message: 'Please supply a valid email address'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your phone number'
                    },
                    phone: {
                        country: 'US',
                        message: 'Please supply a vaild phone number with area code'
                    }
                }
            },
            address: {
                validators: {
                     stringLength: {
                        min: 8,
                    },
                    notEmpty: {
                        message: 'Please supply your street address'
                    }
                }
            },
            city: {
                validators: {
                     stringLength: {
                        min: 4,
                    },
                    notEmpty: {
                        message: 'Please supply your city'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'Please select your state'
                    }
                }
            },
            zip: {
                validators: {
                    notEmpty: {
                        message: 'Please supply your zip code'
                    },
                    zipCode: {
                        country: 'US',
                        message: 'Please supply a vaild zip code'
                    }
                }
            },
            comment: {
                validators: {
                      stringLength: {
                        min: 10,
                        max: 200,
                        message:'Please enter at least 10 characters and no more than 200'
                    },
                    notEmpty: {
                        message: 'Please supply a description of your project'
                    }
                    }
                }
            }
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
});



</script>

    <!-- header -->
    <header id="header" class="clearfix">
        <!-- navbar -->
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- navbar-header -->
                <div class="navbar-header">
                    
                    <a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
                </div>
                <!-- /navbar-header -->
                
                <div class="navbar-left">
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a>
                                <!-- <ul class="dropdown-menu">
                                    <li class="active"><a href="index-2.php">Home Default </a></li>
                                    <li><a href="index-one.php">Home Page V-1</a></li>
                                    <li><a href="index-two.php">Home Page V-2</a></li>
                                    <li><a href="index-three.php">Home Page V-3</a></li>
                                    <li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
                                    <li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
                                </ul> -->
                            </li>
                            <!-- <li><a href="index-one.php">Category</a></li> -->
                            <li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
                            <li><a href="faq.php">Support</a></li> 
                            <!-- <li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="ad-post.php">Ad post</a></li>
                                    <li><a href="details.php">Ad Details</a></li>
                                    <li><a href="my-ads.php">My Ads</a></li>
                                    <li><a href="my-profile.php">My Profile</a></li>
                                    <li><a href="favourite-ads.php">Favourite Ads</a></li>
                                    <li><a href="archived-ads.php">Archived Ads</a></li>
                                    <li><a href="pending-ads.php">Pending Ads</a></li>
                                    <li><a href="delete-account.php">Close Account</a></li>
                                    <li><a href="published.php">Ad Publised</a></li>
                                    <li><a href="coming-soon.php">Coming Soon <span class="badge">New</span></a></li> -->
                                    <!-- <li><a href="pricing.php">Pricing</a></li> -->
                                    <!-- <li><a href="500-page.php">500 Opsss<span class="badge">New</span></a></li>
                                    <li><a href="404-page.php">404 Error<span class="badge">New</span></a></li>
                                </ul>
                            </li> -->
                            <li><a href="about-us.php">ABout Us</a></li>
                            <li><a href="contact-us.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
                
                <!-- nav-right -->
                <div class="nav-right">
                <?php if($_SESSION){?>      
                    <ul class="sign-in noscreen-res">
                        <li>
                            <dropdown class="dropdown-toggle" data-toggle="dropdown"><a href="#"><?php echo $fname ?>&nbsp;<span class="caret"></span></a></dropdown><font style="color: #ffffff; font-weight: normal;">&nbsp;&nbsp;|&nbsp;&nbsp;<a href="logout.php">Log Out</a></font>
                            <ul class="dropdown-menu">
                              <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-ads.php">My Ads</a></li>
                              <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">My Profile</a></li>
                              <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">&nbsp;</a></li>
                            </ul>
                        </li>
                    </ul>
                <?php }else{ ?>
                    <ul class="sign-in noscreen-res">
                        <li><a href="signin.php"> Sign In </a></li>
                        <li>&nbsp;<a href=""> | </a></li>
                        <li><a href="signup.php">Register</a></li>
                    </ul>
                <?php } ?>
                    <a href="ad-post-details.php" class="btn btn-post">Post Your Ad!</a>
                </div>
                <!-- nav-right -->
            </div><!-- container -->
        </nav><!-- navbar -->
    </header><!-- header -->

    <!--mobile screen nav-right start-->
    <div class="nav-second">
    <?php if($_SESSION){?>      
        <ul class="sign-in noscreenmin">
            <li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
            <li class="border-right"><a href="contact-us.php"> <img class="user-icon" src="images/icon/icon-call.png"/> </a></li>
            <li class="border-right"><a href="my-profile.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
            <li><a href="logout.php"> <img class="user-icon" src="images/icon/icon-logout.png"/> </a></li>
        </ul>
    <?php }else{ ?>
        <ul class="sign-in noscreenmin">
            <li class="pull-left ads border-right"><a href="categories-main.php?category=0&province=0&pg=0"><i class="icofont icofont-ui-tag"></i> All Ads </a></li>
            <li class="border-right"><a href="contact-us.php"> <img class="user-icon" src="images/icon/icon-call.png"/> </a></li>
            <li> <a href="signin.php"> <img class="user-icon" src="images/icon/icon-user.png"/> </a></li>
        </ul>
    <?php } ?>
    </div>
    <!--mobile screen nav-right end-->

    <!-- main -->
    <section id="main" class="clearfix details-page">
        <div class="container container-back">          
            <!-- main-content -->
            <div class="section slider">                    
                <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12 slider-text2">
            <h3 class="slider-text3"><?php echo $row['ad_tittle']?></h3>
          </div>
          <div class="col-md-12 col-sm-12 col-xs-12 slider-text2">
            <h4 class="slider-text2 text-justify"><?php echo $row['ad_type']?> by <a class="detail-poster" href="#"><?php echo $row['user_first']?> <?php echo $row['user_last']?></a> <?php
                            $date=date_create($row['cre_date']);
                            echo date_format($date,"d M h:i a");
                            ?>, <?php echo $row['sub_cat']?> in <?php echo $row['location']?></a></h4>
          </div>
                    <!-- carousel -->
                    <div class="col-md-7">
                        <div id="product-carousel" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                            <?php
                                $query1 = "SELECT * FROM ads_image WHERE ad_id=".$_REQUEST['id']." LIMIT 5";
                                $result1 = $connection->query($query1);
                                $i=0;   
                                while($row1 = $result1->fetch_assoc()) { 
                                    $bodytag = str_replace("../", "", $row1['url'] );
                            ?>
                                <li data-target="#product-carousel" data-slide-to="<?php echo $i; ?>">
                                    <img src="<?php echo $bodytag ; ?>" alt="Carousel Thumb" class="img-responsive resprint">
                                </li>
                            <?php $i++;}?>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <!-- item -->
                                <?php
                                $query1 = "SELECT * FROM ads_image WHERE ad_id=".$_REQUEST['id'];
                                $result1 = $connection->query($query1);
                                $i=0;   
                                while($row1 = $result1->fetch_assoc()) { 
                                    $bodytag = str_replace("../", "", $row1['url'] );
                                ?>
                                <div class="item <?php if($i==0){ echo 'active';} ?>">
                                    <div class="carousel-image">
                                        <!-- image-wrapper -->
                                        <img src="<?php echo $bodytag;?>" alt="Featured Image" class="img-responsive">
                                    </div>
                                </div>
                                <?php $i++;}?>
                                
                            </div><!-- carousel-inner -->

                            <!-- Controls -->
                            <a class="left carousel-control" href="#product-carousel" role="button" data-slide="prev">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                            <a class="right carousel-control" href="#product-carousel" role="button" data-slide="next">
                                <i class="fa fa-chevron-right"></i>
                            </a><!-- Controls -->
                        </div>
                    </div><!-- Controls -->

                    <!-- slider-text -->
                    <div class="col-md-5">
                        <div class="slider-text">
                            <div class="col-md-12 col-sm-12">
                                <div class="ui-price-tag price-text price-row">
                                    <span class="l1 ui-price"></span>
                                    <span class="l2"></span>
                                    <span class="price-row price-text">
                                      Rs <?php echo number_format($row['ad_price'])?>
                                    </span>
                                    <span class="r1"></span>
                                    <span class="r2"></span>
                                    <span class="r3"></span>
                                </div>

                                <span class="price-type">
                                    <?php echo $row['price_type']?>
                                </span>
                            </div>

                              <!-- <div class="contact-with col-md-12 col-sm-12 noprint">
                                <span class="show-number">
                                  <a class="contact-btn slider-text2">
                                    <span class="price col-md-12 col-sm-12 col-xs-12 hide-text">
                                    <img src="images/icon/phone.png" class="img-detail-ico">Click for phone number</span>
                                  </a>
                                  <a href="tel:<?php echo ($row['mob_no'])?>">
                                    <span class="price col-md-12 col-sm-12 col-xs-12 hide-number number">
                                      <?php echo ($row['mob_no'])?>
                                    </span>
                                  </a>
                                </span>
                                <span class="show-number">
                                  <a href="tel:<?php echo ($row['tel_no'])?>">
                                    <span class="price col-md-12 col-sm-12 col-xs-12 hide-number number">
                                      <?php echo ($row['tel_no'])?>
                                    </span>
                                  </a>
                                </span>
                              </div> -->

                              <div class="col-md-12 col-sm-12 noprint margin-top-20 margin-bottom-20">
                                <a href="#contact-email" class="contact-btn">
                                  <span class="price col-md-12 col-sm-12 col-xs-12 price-bottom">
                                    <img src="images/icon/cart.png" class="img-detail-ico">Deliver to your doorstep</span>
                                </a>
                              </div>

                              <div class="noscreen noscreenmin noscreen-res">
                                  <?php echo ($row['mob_no'])?>
                              </div>
                              <div class="noscreen noscreenmin noscreen-res">
                                  <?php echo ($row['tel_no'])?>
                              </div>

                              <!-- <div class="contact-with col-md-12" style="margin-top:10px;">
                                <h4>Contact with </h4>
                                  <span class="btn btn-red show-number col-md-12 col-xs-12">
                                    <i class="fa fa-phone-square"></i>
                                    <span class="hide-text">Get phone number</span> 
                                    <span class="hide-number"><?php echo $row['mob_no']?>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $row['tel_no']?></span>
                                  </span>
                                <a href="#" class="btn col-md-12 col-xs-12"><i class="fa fa-envelope-square"></i><?php echo $row['email']?></a>
                              </div> -->

                              <!-- <div class="price text-center noprint">
                                <div class="social-links">
                                  <ul class="list-inline">
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//classify.lk/details.php?id=<?php echo $row['ad_id']?>" target="_blank"><i class="fa fa-facebook-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
                                    <li><a href="#"><i class="fa fa-tumblr-square"></i></a></li>
                                  </ul>
                                </div>
                              </div> -->

                              <div class="col-md-12 col-xs-12 noprint margin-top-20">
                                 <iframe src="<?php echo ($row['video_url'])?>" width="100%" height="250"></iframe> 
                              </div>
                        </div>
                    </div><!-- slider-text -->
                </div>              
            </div><!-- slider -->


            <div class="description-info col-xs-12 col-md-12">
                <section id="contact-email" class="row">
                    <div class="col-md-4 description-info noprint" style="padding-right: 0 !important;">
                        <?php if($row['delivery_option']==1){?>
                        <div class="short-info" style="background-color: #d7d7d7;">
                          <h4 class="col-md-12 text-center">Place your Delivery Order!</h4>
                          <form action="deals-send-email.php" method="post">
                            <div class="">
                              <input type="text" name="item_name" id="item_name" class="form-control contact-email-text text-place-gray text-center" placeholder="Item name" readonly value="<?php echo $row['ad_tittle']?>">
                            </div>
                            <div class="">
                              <input type="text" name="item_qty" id="item_qty" class="form-control contact-email-text text-place-gray text-center" placeholder="order qty" value="1">
                            </div>
                            <div class="">
                              <input type="text" name="orderer_name" id="orderer_name" class="form-control contact-email-text text-place-gray" placeholder="your name" required="required">
                            </div>
                            <div class="">
                              <input type="email" name="orderer_email" id="orderer_email" class="form-control contact-email-text email-place-gray" placeholder="your email">
                            </div>
                            <div class="">
                              <input type="text" name="orderer_phone" id="orderer_phone" class="form-control contact-email-text text-place-gray" placeholder="your phone number" required="required">
                            </div>
                            <div class="">
                              <textarea name="orderer_address" id="orderer_address" class="form-control contact-email-textarea textarea-place-gray" placeholder="Address for Delivery" required="required"></textarea>
                            </div>
                            <div class="">
                              <a href="deals-send-email.php">
                                <button class="btn btn-submit-email">Submit your request!</button>
                              </a>
                            </div>
                          </form>
                        </div>
                        <?php }else{?>
                        <div class="short-info col-md-12">
                          <h4 class="col-md-12">Contact via email</h4>
                          <form>
                            <div class="col-md-12">
                              <input type="text" class="form-control contact-email-text" placeholder="your name here">
                            </div>
                            <div class="col-md-12">
                              <input type="email" class="form-control contact-email-text" placeholder="your email address here">
                            </div>
                            <div class="col-md-12">
                              <input type="text" class="form-control contact-email-text" placeholder="your phone number here">
                            </div>
                            <div class="col-md-12">
                              <textarea class="form-control contact-email-textarea" placeholder="your message here"></textarea>
                            </div>
                            <div class="col-md-12">
                              <a id="next1" data-toggle="tab" href="#advertiser">
                                <button class="btn btn-submit-email" onclick="submit_deals();">Submit your request!</button>
                              </a>
                            </div>
                          </form>
                        </div><?php }?>
                      </div>

                    <!-- description -->
                    <div class="col-md-5 description-info">                     
                        <div class="short-info description-detail">
                            <h4>Description</h4>
                            <p class="description-detail text-justify"><?php echo nl2br($row['description'])?></p>
                        </div>
                    </div>
                    <!-- description -->

                    <!-- description-short-info -->
                    <div class="col-md-3 description-info noprint">                 
                        <div class="short-info">
                            <h4>Additional Infomation</h4>
                            <ul>
                                <li><span class="additional-text">Address</span> : <a href="#"><?php echo $row['address']?></a></li>
                                <li><span class="additional-text">Delivery</span> : <?php if($row['delivery_option']==1){?><a href=""> Cash on Delivery</a><?php }else{?><a href=""> Meet in Person</a><?php }?></li>
                                <!-- <li><span class="additional-text">More ads by</span> : <span><a href="#"><?php echo $row['user_first']?> <?php echo $row['user_last']?></span></a></li> -->
                                <li><span class="additional-text">Posted by</span> : <span><a href="#"><?php echo $row['user_first']?> <?php echo $row['user_last']?></span></a></li>
                                <li><a onclick="PrintPage()"><i class="fa fa-print hover-color"></i>Print this ad</a></li>
                                <li><a href="#"><i class="fa fa-reply hover-color"></i>Send to a friend</a></li>
                                <li><a href="#"><i class="fa fa-star-o hover-color"></i>Save ad as Favorite</a></li>
                                <li><a href="#"><i class="fa fa-ban hover-color"></i>Report this ad</a></li>
                                <li><a href="#"><i class="fa fa-eye hover-color"></i>0000<?php echo $result_count['total']; ?></a></li>
                            </ul>
                        </div>
                    </div>
                </section><!-- row -->
            </div><!-- description-info --> 
            <?php }?>
        </div><!-- container -->
    </section><!-- main -->
    
    <!-- cta -->
            <div class="section cta text-center noprint" style="margin-bottom: 50px;">
                <div class="row">
                    <!-- single-cta -->
                    <div class="col-sm-4">
                        <div class="single-cta">
                            <!-- cta-icon -->
                            <div class="cta-icon icon-secure">
                                <img src="images/icon/13.png" alt="Icon" class="img-responsive">
                            </div><!-- cta-icon -->

                            <h4>Secure Trading</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                        </div>
                    </div><!-- single-cta -->

                    <!-- single-cta -->
                    <div class="col-sm-4">
                        <div class="single-cta">
                            <!-- cta-icon -->
                            <div class="cta-icon icon-support">
                                <img src="images/icon/14.png" alt="Icon" class="img-responsive">
                            </div><!-- cta-icon -->

                            <h4>24/7 Support</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                        </div>
                    </div><!-- single-cta -->

                    <!-- single-cta -->
                    <div class="col-sm-4">
                        <div class="single-cta">
                            <!-- cta-icon -->
                            <div class="cta-icon icon-trading">
                                <img src="images/icon/15.png" alt="Icon" class="img-responsive">
                            </div><!-- cta-icon -->

                            <h4>Easy Trading</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                        </div>
                    </div><!-- single-cta -->
                </div>
            </div><!-- cta -->

    <!-- footer -->
    <footer id="footer" class="clearfix">
        
        <div class="footer-bottom clearfix text-center">
            <div class="container">
                <p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
            </div>
        </div><!-- footer-bottom -->
    </footer><!-- footer -->
    
     <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script src="js/gmaps.min.js"></script>
    <script src="js/goMap.js"></script>
    <script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script>
    <script src="js/jquery.countdown.js"></script>    
    <script src="js/custom.js"></script>
    <script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

function submit_email() {
    alert("Function is temporary out of service!");
}

function submit_deals() {
    alert("Send a message to 0713399099 via whatsapp!");
}

function PrintPage() {
    window.print();
}
</script>
  </body>
</html>