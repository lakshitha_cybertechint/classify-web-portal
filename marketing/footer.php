<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="css/cart_style.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrapTheme.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="admin_style.css" rel="stylesheet">
	<link href="../owl-carousel/owl.theme.css" rel="stylesheet">
	<link rel="icon" type="images/png" sizes="96x96" href="images/favicon.png">
</head>
<body id="body_bg" topmargin='8' leftmargin='85' marginwidth='85'>

<div class="panel-footer text-center" style="">
  Powered & &copy; 2016 All Rights Reserved @ <a href="http://www.cybertech.lk" target="_blank">Cybertech International (pvt) Ltd</a>. Virsion 1.0.1
</div>

</body>

</html>