<!DOCTYPE html>
<html>
<head>
	<title>Send SMS | CyberMarketing | Cybertech International (pvt) Ltd</title>
	<link href="css/cart_style.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrapTheme.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="admin_style.css" rel="stylesheet">
	<link href="../owl-carousel/owl.theme.css" rel="stylesheet">
	<link rel="icon" type="images/png" sizes="96x96" href="images/favicon.png">
</head>
<body id="body_bg" topmargin='8' leftmargin='85' marginwidth='85'>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" style="color: #Ff0000;" href="#">Mailsender</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav" style="margin-top: 6px;">
        <li><a href="index.php">Home</a></li>
        <li><a href="mailSender.php">E-mail Sender</a></li>
        <li class="active"><a href="smsSender.php">SMS Sender<span class="sr-only">(current)</span></a></li>
        <li><a href="socialMedia.php">Social Media</a></li>
        <li><a href="settings.php">Settings</a></li>
        
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><label style="margin-top: 11px;">Welcome</label></li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>

</html>