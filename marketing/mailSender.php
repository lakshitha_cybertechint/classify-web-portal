<!DOCTYPE html>
<html>
<head>
	<title>Index | Admin</title>
	<link href="css/cart_style.css" rel="stylesheet" type="text/css" />
	<link href="assets/css/bootstrapTheme.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="admin_style.css" rel="stylesheet">
	<link href="../owl-carousel/owl.theme.css" rel="stylesheet">
	<link rel="icon" type="images/png" sizes="96x96" href="images/favicon.png">
</head>
<body id="body_bg" topmargin='8' leftmargin='85' marginwidth='85'>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" style="color: #Ff0000;" href="#">Mailsender</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav" style="margin-top: 6px;">
        <li><a href="index.php">Home</a></li>
        <li class="active"><a href="mailSender.php">E-mail Sender<span class="sr-only">(current)</span></a></li>
        <li><a href="smsSender.php">SMS Sender</a></li>
        <li><a href="socialMedia.php">Social Media</a></li>
        <li><a href="settings.php">Settings</a></li>
        
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><label style="margin-top: 11px;">Welcome</label></li>
        
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>

  <div class="panel-body" >
    <pre class="bg-danger col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><h4><b>Use this form for send Emails</b></h4></pre>

  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:50px;">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <input type="text" class="form-control" name="cat_name" id="cat_name"
      placeholder="Sender's Name should be shown as" required="required">
    </div>
  
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <input type="text" class="form-control" name="cat_name" id="cat_name" placeholder="Sender's email address should be shown as" required="required">
    </div>
  </div>

  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0px;">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <input type="text" class="form-control" name="cat_name" id="cat_name"
      placeholder="recipient's Name should be shown as" required="required">
    </div>
  
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      <input type="text" class="form-control" name="cat_name" id="cat_name" placeholder="Reply email address should be shown as (optional)">
    </div>
  </div>

  <!-- <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <span class="input-group-addon">
        <input type="checkbox" aria-label="Checkbox for following text input">
      </span>
      <input type="text" class="form-control" name="cat_name" id="cat_name" placeholder="Enter Email Subject" required="required">
    </div>
  </div> -->
  
<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top:0px;">
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    <div class="input-group">
      <span class="input-group-addon">
        <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
      </span>
      <input type="text" class="form-control" placeholder="Enter " aria-label="Text input with checkbox">
    </div>
  </div>
  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
    <div class="input-group">
      <span class="input-group-addon">
        <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
      </span>
      <input type="file" class="form-control">
    </div>
  </div>
</div>

  <div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" padding-bottom:100px;">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <textarea class="form-control" placeholder="Enter Email body" rows="10" id="comment"></textarea>
    </div>
  </div>

<script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<?php include('footer.php')
?>

</body>

</html>