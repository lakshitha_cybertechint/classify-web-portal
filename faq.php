<?php session_start();
 include 'connection.php';
?>
<?php 
$fname="";
if($_SESSION){
	$sql = "select * From user where id='".$_SESSION['user_id']."'" ; 
	$result = mysqli_query($connection,$sql);
	if(mysqli_num_rows($result)>0){
	   while($row = mysqli_fetch_assoc($result)){
	   	   $fname=$row['first_name'] ;
	   }
	}
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Theme Region">
   	<meta name="description" content="">

    <title>Trade | World's Largest Classifieds Portal</title>

   <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/icofont.css">
    <link rel="stylesheet" href="css/owl.carousel.css">  
    <link rel="stylesheet" href="css/slidr.css">     
    <link rel="stylesheet" href="css/main.css">  
	<link id="preset" rel="stylesheet" href="css/presets/preset1.css">	
    <link rel="stylesheet" href="css/responsive.css">
	
	<!-- font -->
	<link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700,300' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>

	<!-- icons -->
	<link rel="icon" href="images/ico/favicon.ico">	
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="images/ico/apple-touch-icon-57-precomposed.png">
    <!-- icons -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Template Developed By ThemeRegion -->
  </head>
  <body>
	<!-- header -->
	<header id="header" class="clearfix">
		<!-- navbar -->
		<nav class="navbar navbar-default">
			<div class="container">
				<!-- navbar-header -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="index.php"><img class="img-responsive" style="margin-top: -10px;" src="images/logo.png" alt="Logo"></a>
				</div>
				<!-- /navbar-header -->
				
				<div class="navbar-left">
					<div class="collapse navbar-collapse" id="navbar-collapse">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a>
								<!-- <ul class="dropdown-menu">
									<li class="active"><a href="index-2.php">Home Default </a></li>
									<li><a href="index-one.php">Home Page V-1</a></li>
									<li><a href="index-two.php">Home Page V-2</a></li>
									<li><a href="index-three.php">Home Page V-3</a></li>
									<li><a href="index-car.php">Home Page V-4<span class="badge">New</span></a></li>
									<li><a href="index-car-two.php">Home Page V-5<span class="badge">New</span></a></li>
								</ul> -->
							</li>
							<!-- <li><a href="index-one.php">Category</a></li> -->
							<li><a href="categories-main.php?category=0&province=0&pg=0">all ads</a></li>
							<li class="active"><a href="faq.php"><strong>Support</strong></a></li>
							<li><a href="about-us.php">ABout Us</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				
				<!-- nav-right -->
				<div class="nav-right">
				<?php if($_SESSION){?>		
					<ul class="sign-in">
						<li>
							<dropdown class="dropdown-toggle" data-toggle="dropdown"><a href="#"><?php echo $fname ?>&nbsp;<span class="caret"></span></a></dropdown><font style="color: #ffffff; font-weight: normal;">&nbsp;&nbsp;|&nbsp;&nbsp;<a href="logout.php">Log Out</a></font>
						    <ul class="dropdown-menu">
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-ads.php">My Ads</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">My Profile</a></li>
						      <li><a class="page-scroll" style="color: #000000; font-weight: 550;" href="my-profile.php">&nbsp;</a></li>
						    </ul>
						</li>
					</ul>
						<?php }else{ ?>
					<ul class="sign-in">
						<li><a href="signin.php"> Sign In </a></li>
						<li>&nbsp;<a href=""> | </a></li>
						<li><a href="signup.php">Register</a></li>
					</ul>
						<?php } ?>
					<a href="ad-post-details.php" class="btn btn-post">Post Your Ad!</a>
				</div>
				<!-- nav-right -->
			</div><!-- container -->
		</nav><!-- navbar -->
	</header><!-- header -->

	<section id="main" class="clearfix page">
		<div class="container">
			<div class="faq-page">
								
				<div class="accordion">
					<div class="panel-group" id="accordion">			

						<div class="panel panel-default panel-faq">
							<div class="panel-heading active-faq">
								<a data-toggle="collapse" data-parent="#accordion" href="#faq-one">
									<h4 class="panel-title">
									Claritas est etiam processus? 
									<span class="pull-right"><i class="fa fa-minus"></i></span>
									</h4>
								</a>
							</div><!-- panel-heading -->

							<div id="faq-one" class="panel-collapse collapse collapse in">
								<div class="panel-body">
									<p>Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla</p>
									<p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum</p>
									<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi</p>
								</div>
							</div>
						</div><!-- panel -->

						<div class="panel panel-default panel-faq">
							<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#faq-two">
									<h4 class="panel-title">
									Vel illum dolore eu? 
									<span class="pull-right"><i class="fa fa-plus"></i></span>
									</h4>
								</a>
							</div><!-- panel-heading -->

							<div id="faq-two" class="panel-collapse collapse">
								<div class="panel-body">
									<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla</p>
									<p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl</p>	
								</div>
							</div>
						</div><!-- panel -->

						<div class="panel panel-default panel-faq">
							<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#faq-three">
									<h4 class="panel-title">
									Nam liber tempor cum soluta? 
									<span class="pull-right"><i class="fa fa-plus"></i></span>
									</h4>
								</a>
							</div><!-- panel-heading -->

							<div id="faq-three" class="panel-collapse collapse">
								<div class="panel-body">
									<p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>
									<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>									
									<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi,</p>
								</div>
							</div>
						</div><!-- panel -->

						<div class="panel panel-default panel-faq">
							<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#faq-four">
									<h4 class="panel-title">
									Claritas est etiam processus dynamicus? 
									<span class="pull-right"><i class="fa fa-plus"></i></span>
									</h4>
								</a>
							</div><!-- panel-heading -->

							<div id="faq-four" class="panel-collapse collapse">
								<div class="panel-body">
									<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>									
									<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi,</p>
								</div>
							</div>
						</div><!-- panel -->

						<div class="panel panel-default panel-faq">
							<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#faq-five">
									<h4 class="panel-title">
									Duis autem vel eum iriure dolor? 
									<span class="pull-right"><i class="fa fa-plus"></i></span>
									</h4>
								</a>
							</div><!-- panel-heading -->

							<div id="faq-five" class="panel-collapse collapse">
								<div class="panel-body">
									<p>p ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option con</p>
									<p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>									
									<p>Claritas est etiam processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi,</p>
								</div>
							</div>
						</div><!-- panel -->

						<div class="panel panel-default panel-faq">
							<div class="panel-heading">
								<a data-toggle="collapse" data-parent="#accordion" href="#faq-six">
									<h4 class="panel-title">
									Mirum est notare quam littera gothica? 
									<span class="pull-right"><i class="fa fa-plus"></i></span>
									</h4>
								</a>
							</div><!-- panel-heading -->

							<div id="faq-six" class="panel-collapse collapse">
								<div class="panel-body">
									<p>Velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option con</p>
									<p>Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius.</p>									
									<p>Mirum est notare quam littera gothica, quam nunc putamus parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi,</p>
								</div>
							</div>
						</div><!-- panel -->
					</div>
				</div>
				
			</div><!-- faq-page -->
		</div><!-- container -->
		<section id="something-sell" class="clearfix parallax-section">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 text-center">
						<h2 class="title">Did Not find your answer yet? Still need help ?</h2>
						<h4>Send us a note to Help Center</h4>
						<a href="contact-us.php" class="btn btn-primary">Contact Us</a>
					</div>
				</div><!-- row -->
			</div><!-- contaioner -->
		</section><!-- something-sell -->
	</section>
	
	<!-- footer -->
	<footer id="footer" class="clearfix">
		
		<div class="footer-bottom clearfix text-center">
			<div class="container">
				<p>Copyright &copy; 2016-<?php echo date("Y");?>. Powered by <a href="http:www.cybertech.lk" target="_blank">Cybertech Internationals (pvt) Ltd</a></p>
			</div>
		</div><!-- footer-bottom -->
	</footer><!-- footer -->
	
    <!-- JS -->
    <script src="js/jquery.min.js"></script>
    <script src="js/modernizr.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
	<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
	<script src="js/gmaps.min.js"></script>
	<script src="js/goMap.js"></script>
	<script src="js/map.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/smoothscroll.min.js"></script>
    <script src="js/scrollup.min.js"></script>
    <script src="js/price-range.js"></script> 
    <script src="js/jquery.countdown.js"></script>   
    <script src="js/custom.js"></script>
	<script src="js/switcher.js"></script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-89509903-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
  </body>
</html>